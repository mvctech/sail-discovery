import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Step1Component} from './steps/step1/step1.component';
import {Step2Component} from './steps/step2/step2.component';
import {Step3Component} from './steps/step3/step3.component';
import {Step4Component} from './steps/step4/step4.component';
import {Step5Component} from './steps/step5/step5.component';
import {Step6Component} from './steps/step6/step6.component';
import {Step7Component} from './steps/step7/step7.component';
import {Step8Component} from './steps/step8/step8.component';
import {Step9Component} from './steps/step9/step9.component';
import {Step10Component} from './steps/step10/step10.component';
import {Step11Component} from './steps/step11/step11.component';
import {Step12Component} from './steps/step12/step12.component';
import {Step13Component} from './steps/step13/step13.component';
import {Step14Component} from './steps/step14/step14.component';
import {Step15Component} from './steps/step15/step15.component';
import {Step16Component} from './steps/step16/step16.component';
import {Step17Component} from './steps/step17/step17.component';
import {Step18Component} from './steps/step18/step18.component';
import {Step19Component} from './steps/step19/step19.component';
import {Step20Component} from './steps/step20/step20.component';
import {Step21Component} from './steps/step21/step21.component';
import {Step22Component} from './steps/step22/step22.component';
import {Step23Component} from './steps/step23/step23.component';
import {Step24Component} from './steps/step24/step24.component';
import {Step25Component} from './steps/step25/step25.component';
import {Step26Component} from './steps/step26/step26.component';
import {HeaderComponent} from './shared/header/header.component';
import {FooterComponent} from './shared/footer/footer.component';
import {ButtonComponent} from './shared/button/button.component';
import {ProgressComponent} from './shared/progress/progress.component';
import {CollapsibleComponent} from './shared/collapsible/collapsible.component';
import {CoreModule} from './core/core.module';
import { NotificationMessageComponent } from './shared/notification-message/notification-message.component';
import { LoadingComponent } from './shared/loading/loading.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ButtonComponent,
    ProgressComponent,
    CollapsibleComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    Step4Component,
    Step5Component,
    Step6Component,
    Step7Component,
    Step8Component,
    Step9Component,
    Step10Component,
    Step11Component,
    Step12Component,
    Step13Component,
    Step14Component,
    Step15Component,
    Step16Component,
    Step17Component,
    Step18Component,
    Step19Component,
    Step20Component,
    Step21Component,
    Step22Component,
    Step23Component,
    Step24Component,
    Step25Component,
    Step26Component,
    NotificationMessageComponent,
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    CoreModule.forRoot()
  ],
  providers: [
    HttpClientModule
  ],
  bootstrap: [AppComponent],
  exports: [HeaderComponent, ButtonComponent]
})
export class AppModule {
}
