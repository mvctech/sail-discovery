import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//
import { ConfigService } from '../../core/config/config.service';

@Injectable()
export class HttpRequestService {
    private headers: Headers;

    constructor(private _http: Http, private _config: ConfigService) {
        this.headers = new Headers();
    }

    /**
     * GET Http Request without token
     * Common service to handle PUT Http Request
     * @param {string} url - Get request end point URL.
     * @return {Observable}
     */
    getHttpRequestWithoutToken(url: string) {
        this.headers.set('Content-Type', 'application/json');
        return this._http
            .get(this._config.getBaseURL() + url, { headers: this.headers })
            .map(this.successResponse)
            .catch(this.errorResponse);
    }

    /**
     * POST Http Request without token
     * Common service to handle PUT Http Request
     * @param {string} url - POST request with end point URL.
     * @param {string} data - POST request with data.
     * @return {Observable}
     */
    postHttpRequestWithoutToken(data: string, url: string) {
        this.headers.set('Content-Type', 'application/json');
        return this._http
            .post(this._config.getBaseURL() + url, data, { headers: this.headers })
            .map(this.successResponse)
            .catch(this.errorResponse);
    }

    postHttpRequestWithoutTokenJson(data: string, url: string) {
        const self = this;
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        const body = data;
        return self._http
            .post(this._config.getBaseURL() + url, body, options)
            .map(this.successResponse)
            .catch(this.errorResponse);
    }


    /**
     * PUT Http Request without token
     * Common service to handle PUT Http Request
     * @param {string} url - PUT request with end point URL.
     * @param {string} data - PUt request with data.
     * @return {Observable}
     */
    putHttpRequestWithoutToken(data: string, url: string) {
        this.headers.set('Content-Type', 'application/json');
        return this._http
            .put(this._config.getBaseURL() + url, data, { headers: this.headers })
            .map(this.successResponse)
            .catch(this.errorResponse);

    }
    /**
     * GET Http Request with token
     * Common service to handle PUT Http Request
     * @param {string} url - Get request end point URL.
     * @return {Observable}
     */
    getHttpRequest(url: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'Bearer ' + localStorage.getItem('token'));
        return this._http
            .get(this._config.getBaseURL() + url, { headers: this.headers })
            .map(this.successResponse)
            .catch(this.errorResponse);
    }

    /**
     * POST Http Request with token
     * Common service to handle PUT Http Request
     * @param {string} url - POST request with end point URL.
     * @param {string} data - POST request with data.
     * @return {Observable}
     */
    postHttpRequest(data: string, url: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'Bearer ' + localStorage.getItem('token'));
        return this._http
            .post(this._config.getBaseURL() + url, data, { headers: this.headers })
            .map(this.successResponse)
            .catch(this.errorResponse);
    }

    /**
     * PUT Http Request with token
     * Common service to handle PUT Http Request
     * @param {string} url - PUT request with end point URL.
     * @param {string} data - PUT request with data.
     * @return {Observable}
     */
    putHttpRequest(data: string, url: string) {
        this.headers.set('Content-Type', 'application/json');
        this.headers.set('Authorization', 'Bearer ' + localStorage.getItem('token'));
        return this._http
            .put(this._config.getBaseURL() + url, data, { headers: this.headers })
            .map(this.successResponse)
            .catch(this.errorResponse);
    }

    /**
     * errorResponse
     * Common method to handle error response
     * @param {Response} error
     * @return {Observable}
     */
    private errorResponse(error: Response) {
        return Observable.throw(error.json() || null);
    }

    /**
     * successResponse
     * Common method to handle success response
     * @param {Response} error
     * @return {Observable}
     */
    private successResponse(result: Response) {
        return result.json() || null;
    }
}
