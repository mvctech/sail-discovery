import { Injectable } from '@angular/core';
//
import * as CryptoJS from 'crypto-js';
import { SHA256 } from 'crypto-js';
//
import { ConfigService } from '../config/config.service';

@Injectable()

export class GlobalService {
    constructor(
        private _config: ConfigService
    ) {
    }

    setSession(key: string, value: any, crypt: boolean) {
        if (crypt) {
            sessionStorage.setItem(key, CryptoJS.AES.encrypt(JSON.stringify(value), this._config.CONFIG_DATA.SALT).toString());
        } else {
            sessionStorage.setItem(key, JSON.stringify(value));
        }
    }

    getSession(key: string, crypt: boolean) {
        if (crypt) {
            return JSON.parse(CryptoJS.AES.decrypt(sessionStorage.getItem(key), this._config.CONFIG_DATA.SALT).toString(CryptoJS.enc.Utf8));
        } else {
            return JSON.parse(sessionStorage.getItem(key));
        }
    }

    setLocal(key: string, value: string, crypt: boolean) {
        if (crypt) {
            localStorage.setItem(key, CryptoJS.AES.encrypt(JSON.stringify(value), this._config.CONFIG_DATA.SALT).toString());
        } else {
            localStorage.setItem(key, JSON.stringify(value));
        }
    }

    getLocal(key: string, crypt: boolean) {
        if (crypt) {
            return JSON.parse(CryptoJS.AES.decrypt(localStorage.getItem(key), this._config.CONFIG_DATA.SALT).toString(CryptoJS.enc.Utf8));
        } else {
            return JSON.parse(localStorage.getItem(key));
        }
    }

    startOver() {
        sessionStorage.clear();
        localStorage.clear();
    }
}
