import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
//
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
//
import { StrainBusiness } from '../strain/strain.business';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    private router: Router;
    private sb: StrainBusiness;
    private injector: Injector;
    constructor(
        private _injector: Injector
    ) {
        this.injector = _injector;
    }

    handleError(error: any) {
        // throw error;
        sessionStorage.clear();
        localStorage.clear();
        if (this.router == null) {
            this.router = this.injector.get(Router);
        }
        if (this.sb == null) {
            this.sb = this.injector.get(StrainBusiness);
        }
        // this.router.navigate(['/errorpage']);
        this.router.navigate(['/']);
    }
}
