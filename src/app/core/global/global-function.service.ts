
export namespace GlobalFunction {
    //
    export let obj: any = null;
    export function throwNull2NonNull(field: string, d: any): never {
        return errorHelper(field, d, 'non-nullable object', false);
    }
    export function throwNotObject(field: string, d: any, nullable: boolean): never {
        return errorHelper(field, d, 'object', nullable);
    }
    export function throwIsArray(field: string, d: any, nullable: boolean): never {
        return errorHelper(field, d, 'object', nullable);
    }
    export function checknumber(d: any, nullable: boolean, field: string): void {
        if (typeof (d) !== 'number' && (!nullable || (nullable && d !== null && d !== undefined))) {
            errorHelper(field, d, 'number', nullable);
        }
    }
    export function checkboolean(d: any, nullable: boolean, field: string): void {
        if (typeof (d) !== 'boolean' && (!nullable || (nullable && d !== null && d !== undefined))) {
            errorHelper(field, d, 'boolean', nullable);
        }
    }
    export function checkstring(d: any, nullable: boolean, field: string): void {
        if (typeof (d) !== 'string' && (!nullable || (nullable && d !== null && d !== undefined))) {
            errorHelper(field, d, 'string', nullable);
        }
    }
    export function checkNull(d: any, field: string): void {
        if (d !== null && d !== undefined) {
            errorHelper(field, d, 'null or undefined', false);
        }
    }
    export function errorHelper(field: string, d: any, type: string, nullable: boolean): never {
        if (nullable) {
            type += ', null, or undefined';
        }
        throw new TypeError('Expected ' + type + ' at ' + field + ' but found:\n' + JSON.stringify(d) + '\n\nFull object:\n' + JSON.stringify(obj));
    }
    //
    export function validateEmail(d: any, field: string): boolean {
        const EMAIL_REGEXP = new RegExp('^[a-zA-Z]([.]?([a-zA-Z0-9_-]+)*)?@([a-zA-Z0-9\-_]+\.)+[a-zA-Z]{2,4}$ ', 'i');
        if (typeof (d) === 'string' && d !== null && d !== undefined) {
            return EMAIL_REGEXP.test(d);
        } else {
            return false;
        }
    }
}
