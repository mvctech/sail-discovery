import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
//
import { GlobalService } from '../global/global.service';
import { ConfigService } from '../config/config.service';
import {
    StrainData,
    UserInfo, LPInfo, SearchFilter,
    AgeGroup, IntakeFrequecy, IntakeMethod, MedicalCondition,
    PositiveEffect, NegativeEffect,
    CannabisProfile, StrainType, ProductType, Strain, StrainList, StrainInfo, LicenseProducer, StrainDosage, StrainId, StrainReview
} from './strain-data.model';
import { StrainDataService } from './strain-data.service';
import { StrainApiService } from './strain-api.service';
//
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class StrainBusiness {

    constructor(
        private _router: Router
        , private _g: GlobalService
        , private _config: ConfigService
        , private _data: StrainDataService
        , private _api: StrainApiService
    ) {
    }

    //
    StrainInit(): Observable<any> {
        return new Observable<any>(observer => {
            const body = 'username=' + encodeURIComponent('')
                + '&grant_type=' + encodeURIComponent(this._config.CONFIG_DATA.GRANT_PASS_TYPE)
                + '&password=' + encodeURIComponent('')
                + '&client_id=' + encodeURIComponent(this._config.CONFIG_DATA.SF_ID);
            this._api.getUserAuthorizationCode(body, this._config.getApiByName('API_TOKEN'))
                .mergeMap(res => {
                    localStorage.setItem(this._config.CONFIG_DATA.GRANT_TOKEN_TYPE, res.access_token);
                    // localStorage.setItem(this._config.CONFIG_DATA.GRANT_REFRESH_TOKEN_TYPE, res.refresh_token);
                    return this._api.getLicencedProducerInfo(JSON.stringify(window.location.pathname.split('/')[1]), this._config.getApiByName('GET_LP_INFO'));
                })
                .mergeMap(res => {
                    this.setUserAndLP(res);
                    return this._api.getMasterInfoDetail(this._config.getApiByName('GET_MASTER_INFO'));
                })
                .subscribe(res => {
                    this.setMasterInfo(res);
                    observer.next(true);
                }, (err) => {
                    observer.next(false);
                });
        });
    }

    private setUserAndLP(res: any) {
        const lpInfo = <LPInfo>{};
        lpInfo.Id = res.LpID ? res.LpID : null;
        lpInfo.Email = res.Email ? res.Email : '';
        lpInfo.Logo = res.LpLogo ? res.LpLogo : '';
        lpInfo.Name = res.Name ? res.Name : ''; // '';
        lpInfo.Role = this._config.CONFIG_DATA.ROLE_LP;
        this._g.setSession(this._config.CONFIG_DATA.SS_LPINFO, '', this._config.CONFIG_DATA.ENCRYPT);
        this._data.setLPInfo(lpInfo);

        const userInfo = <UserInfo>{};
        userInfo.Id = 0;
        userInfo.FirstName = '';
        userInfo.LastName = 'GUEST';
        userInfo.Email = ''; // res.Email ? res.Email : null;
        userInfo.Username = 'PatientUser';
        userInfo.Role = this._config.CONFIG_DATA.ROLE_USER;
        userInfo.Type = 'StrainDiscover';
        this._g.setSession(this._config.CONFIG_DATA.SS_USERINFO, '', this._config.CONFIG_DATA.ENCRYPT);
        this._data.setUserInfo(userInfo);
    }
    private setMasterInfo(res: any) {
        this._data.setStrainReview(<StrainReview>{});
        this._data.setSelectedStrain(<Strain>{});
        this._data.resetSelectedStrainList();
        this._data.resetSelectedStrainIdList();
        this._data.setAgeGroup(res.Age_Range);
        this._data.setIntakeFrequecy(res.CannExperience);
        this._data.setMedicalCondition(res.OMedicalConditions);
        this._data.setIntakeMethod(res.IntakeModes);
        this._data.setPositiveEffect(res.PositiveEffects);
        this._data.setNegativeEffect(res.NegativeEffects);
        this._data.setStrainType(res.ProductTypes);
        this._data.setProductType(res.ProductProfiles);
        this._data.setSearchFilter(res.ProductProfiles, res.ProductTypes, res.UsageReasons, null, 0, this._config.CONFIG_DATA.PAGE_ITEMS);
    }

    //
    GetStrainProfile(medicalCondition: MedicalCondition, offset: number, next: number): Observable<any> {
        return new Observable<any>(observer => {
            const reqParam = {
                'Medical_Condition_ID': medicalCondition.Id,
                'Offset': offset,
                'Next': next,
                'Active': this._data.getSearchFilter().activity.filter(each => each.Title.toLowerCase() === 'active')[0].Selected ? 1 : 0,
                'Relax': this._data.getSearchFilter().activity.filter(each => each.Title.toLowerCase() === 'relaxed')[0].Selected ? 1 : 0,
                'ProductTypeList': this._data.getSearchFilter().productType.filter(each => each.Selected).map(each => each.Id),
                'StrainTypeList': this._data.getSearchFilter().strainType.filter(each => each.Selected).map(each => each.Id),
                'LPId': this._data.getLPInfo().Id,
            };

            this._api.getCannabisProfile(reqParam, this._config.getApiByName('GET_STRAIN_FINDER'))
                .subscribe(res => {
                    if (res.Factors_Active.length > 0 && res.Factors_Relax.length > 0 && res.Products.length > 0) {
                        this.setCannabisProfileData(medicalCondition, res);
                        this.setStrainIds(medicalCondition, res.Products);
                        observer.next(true);
                    } else {
                        observer.next(false);
                    }
                }, err => {
                    observer.next(false);
                });
        });
    }
    private setCannabisProfileData(medicalCondition: MedicalCondition, res: any) {
        if (res.Factors_Active.length > 0) {
            this.setCannabisProfileDataActivate(medicalCondition, res);
        }
        if (res.Factors_Relax.length > 0) {
            this.setCannabisProfileDataRelax(medicalCondition, res);
        }
    }
    private setCannabisProfileDataActivate(medicalCondition: MedicalCondition, res: any) {
        const tmpActivate = <CannabisProfile>{};
        tmpActivate.MedicalCondition = Object.assign({}, medicalCondition);
        tmpActivate.MedicalCondition.Title = medicalCondition.Title + ' when activating';
        tmpActivate.Cannabinoid = res.Factors_Active
            .filter(each => each.Terepenes_ID === 0)
            .sort((a, b) => {
                if (a.Priority > b.Priority) { return -1; } else if (a.Priority < b.Priority) { return 1; } else { return 0; }
            })
            .map(each => each.type.trim())
            .splice(0, 3);
        tmpActivate.Terpene = res.Factors_Active
            .filter(each => each.Cannabinoid_ID === 0)
            .sort((a, b) => {
                if (a.Priority > b.Priority) { return -1; } else if (a.Priority < b.Priority) { return 1; } else { return 0; }
            })
            .map(each => each.type.trim())
            .splice(0, 3);
        this._data.setCannabisProfile(tmpActivate);
    }
    private setCannabisProfileDataRelax(medicalCondition: MedicalCondition, res: any) {
        const tmpRelax = <CannabisProfile>{};
        tmpRelax.MedicalCondition = Object.assign({}, medicalCondition);
        tmpRelax.MedicalCondition.Title = medicalCondition.Title + ' when relaxing';
        tmpRelax.Cannabinoid = res.Factors_Relax
            .filter(each => each.Terepenes_ID === 0)
            .sort((a, b) => {
                if (a.Priority > b.Priority) { return -1; } else if (a.Priority < b.Priority) { return 1; } else { return 0; }
            })
            .map(each => each.type.trim())
            .splice(0, 3);
        tmpRelax.Terpene = res.Factors_Relax
            .filter(each => each.Cannabinoid_ID === 0)
            .sort((a, b) => {
                if (a.Priority > b.Priority) { return -1; } else if (a.Priority < b.Priority) { return 1; } else { return 0; }
            })
            .map(each => each.type.trim())
            .splice(0, 3);
        this._data.setCannabisProfile(tmpRelax);
    }
    private setStrainIds(medicalCondition: MedicalCondition, productList: any) {
        const tmp = <StrainList>{ MedicalCondition: <MedicalCondition>{}, StrainIds: new Array<StrainId>(), Total: 0 };
        tmp.MedicalCondition = Object.assign({}, medicalCondition);
        tmp.Total = productList[0].Total;
        //
        productList.map(each => {
            tmp.StrainIds.push({
                ProductId: each.ProductID,
                Activate: each.Active,
                Relax: each.Relax
            });
        });
        this._data.setStrainList(tmp);
    }

    GetStrainList(medicalCondition: MedicalCondition, offset: number, next: number): Observable<any> {

        return new Observable<any>(observer => {
            const reqParam = {
                'Medical_Condition_ID': medicalCondition.Id,
                'Offset': offset,
                'Next': next,
                'Active': this._data.getSearchFilter().activity.filter(each => each.Title.toLowerCase() === 'active')[0].Selected ? 1 : 0,
                'Relax': this._data.getSearchFilter().activity.filter(each => each.Title.toLowerCase() === 'relaxed')[0].Selected ? 1 : 0,
                'ProductTypeList': this._data.getSearchFilter().productType.filter(each => each.Selected).map(each => each.Id),
                'StrainTypeList': this._data.getSearchFilter().strainType.filter(each => each.Selected).map(each => each.Id),
                'LPId': this._data.getLPInfo().Id,
            };
            this._api.getCannabisProfile(reqParam, this._config.getApiByName('GET_STRAIN_FINDER'))
                .mergeMap(res => {
                    this.setStrainIds(medicalCondition, res.Products);
                    if (res && res.Products.length > 0) {
                        this.setStrainIds(medicalCondition, res.Products);
                        const reqParamIDsActive = {
                            'ProductIDs': res.Products.filter(each => typeof (each.ProductID) === 'number').map(each => each.ProductID),
                        };
                        return this._api.getStrainList(reqParamIDsActive, this._config.getApiByName('GET_STRAIN_LIST'));
                    } else {
                        this.StrainBusinessRtn(null);
                    }
                })
                .subscribe(res => {
                    if (res !== null) {
                        const tmpList = this._data.getStrainList().filter(each => each.MedicalCondition.Id === medicalCondition.Id)[0].StrainIds;
                        observer.next(this.getStrainListData(res, tmpList));
                    }
                }, err => {
                    observer.next(null);
                });
        });
    }
    private getStrainListData(res: any, list: StrainId[]): Strain[] {
        const tmpStrains = [];
        res.StrainList.map(each => {
            if (typeof (each.Product_ID) === 'number') {
                const tmpStrainId = (list === null) ? null : list.filter(data => data.ProductId === each.Product_ID)[0];
                tmpStrains.push({
                    Id: each.ID,
                    ProductId: each.Product_ID,
                    ProductName: each.Product_Name,
                    ProductTypeId: each.Profile,
                    ProductType: <ProductType>{ Id: each.Profile, Type: each.ProfileTitle, Image: each.ProfileIcon, Color: each.ProfileColor },
                    StrainTypeId: each.Product_Type,
                    StrainType: <StrainType>{ Id: each.Product_Type, Type: each.TypeTitle, Color: each.Color },
                    ProductLogo: '',
                    Cannabinoids: [],
                    Terpenes: [],
                    LicenseProducerId: each.LPID,
                    LPEmail: each.LP_Email,
                    LPFirstName: each.LP_FirstName,
                    LPLastName: each.LP_LastName,
                    LicenseProducer: <LicenseProducer>{ LpName: each.Store_Name, LpLogo: each.LPImage, Address: each.Address, PostCode: each.Postal_Code, City: each.City, State: each.Province, Country: each.Country, Phone: each.Work_Phone },
                    Activate: tmpStrainId === null ? '' : tmpStrainId.Activate,
                    Relax: tmpStrainId === null ? '' : tmpStrainId.Relax,
                    StrainDosage: null
                });
            }
        });
        //
        tmpStrains.map(each => {
            const t1 = res.CannabinoidList.filter(data => data.Product_ID === each.ProductId);
            if (t1.length > 0) {
                t1.map(data => {
                    each.Cannabinoids.push({ Value: data.Value, Type: data.type });
                });
            }
            const t2 = res.TerepeneList.filter(data => data.Product_ID === each.ProductId);
            if (t2.length > 0) {
                t2.map(data => {
                    each.Terpenes.push({ Value: data.Value, Type: data.type });
                });
            }
        });
        return tmpStrains;
    }

    GetSavedStrainList(): Observable<any> {

        return new Observable<any>(observer => {
            const reqParam = {
                'ProductIDs': this._data.getStrainData().selectedStrainIdList,
            };
            return this._api.getStrainList(reqParam, this._config.getApiByName('GET_STRAIN_LIST'))
                .subscribe(res => {
                    if (res !== null) {
                        observer.next(this.getStrainListData(res, null));
                    } else {
                        observer.next(null);
                    }
                }, err => {
                    observer.next(null);
                });
        });
    }

    // GetLPAddress(): Observable<any> {
    //     return new Observable<any>(
    //         observer => {
    //             const tmpStrain = this._data.getSelectedStrain();
    //             const params: any = {
    //                 'Search_Code': 1, // 1-by id, 2-by name
    //                 'LP_Id': tmpStrain.LicenseProducerId
    //             };
    //             this._api.getLicenseProducers(params, this._config.getApiByName('GET_LP')).subscribe(res => {
    //                 if (res.length > 0 && res[0].address) {
    //                     const tmpLP = <LicenseProducer>{};
    //                     tmpLP.LpId = res[0].LP_Id;
    //                     tmpLP.LpName = res[0].LP_User_Name,
    //                     tmpLP.Address = res[0].address.User_Address;
    //                     tmpLP.City = res[0].address.City;
    //                     tmpLP.Country = res[0].address.Country;
    //                     tmpLP.LpLogo = res[0].Image_Url;
    //                     tmpLP.Phone = res[0].address.Work_Phone;
    //                     tmpLP.PostCode = res[0].address.Postal_Code;
    //                     tmpLP.State = res[0].address.Province;
    //                     tmpStrain.LicenseProducer = tmpLP;
    //                     this._data.setSelectedStrain(tmpStrain);
    //                     observer.next(true);
    //                 } else {
    //                     observer.next(false);
    //                 }
    //             }, err => {
    //                 observer.next(false);
    //             });
    //         });
    // }

    GetSuggestedDosage(productId: number, experienceId: number, activeRelax: number): Observable<any> {
        return new Observable<any>(
            observer => {
                const reqParam = {
                    'Product_ID': productId,
                    'Experience_ID': experienceId,
                    'Active_Relax': activeRelax,
                };
                this._api.getSuggestedDosage(reqParam, this._config.getApiByName('GET_SUGGESTED_DOSAGE')).subscribe(res => {
                    if (res !== null) {
                        const tmp = this._data.getSelectedStrain();
                        const tmpDosage = <StrainDosage>{};
                        tmpDosage.Min = res.Dosage_Min;
                        tmpDosage.Max = res.Dosage_Max;
                        tmpDosage.Scale = res.Scale;
                        tmp.StrainDosage = tmpDosage;
                        this._data.setSelectedStrain(tmp);
                        observer.next(true);
                    } else {
                        observer.next(false);
                    }
                }, err => {
                    observer.next(false);
                });
            });
    }

    SendEmail(notifyMe: boolean, notifyLp: boolean): Observable<any> {
        return new Observable<any>(observer => {
            this.GetSavedStrainList()
                .mergeMap(
                    res => {
                    const reqStrains = [];
                    if (res && res.length > 0) {
                        res.map(each =>
                            reqStrains.push({
                                'Product_Id': each.ProductId,
                                'Product_Name': each.ProductName,
                                'Product_Logo': '',
                                'Product_Type_Id': each.ProductTypeId,
                                'Product_Type': each.ProductType.Type,
                                'Product_Type_Logo': '', // each.ProductType.Image,
                                'Strain_Type_Id': each.StrainTypeId,
                                'Strain_Type': each.StrainType.Type,
                                'Strain_Type_Color': each.StrainType.Color,
                                'Strain_THC': each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'THC').length === 1 ? each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'THC')[0].Value : 0,
                                'Strain_CBD': each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'CBD').length === 1 ? each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'CBD')[0].Value : 0,
                                'LP_Id': each.LicenseProducerId,
                                'LP_Name': each.LicenseProducer ? each.LicenseProducer.LpName : '',
                                'LP_Url': '',
                                'LP_Email': each.LPEmail,
                                'LP_FirstName': each.LPFirstName,
                                'LP_LastName': each.LPLastName,
                            })
                        );
                    }

                    const reqParam = {
                        'CannabisProfile': this._data.getCannabisProfile(),
                        'StrainIds': this._data.getSelectedStrainList().map(each => each.ProductId),
                        'Strains': reqStrains,
                        'UserEmail': this._data.getUserInfo().Email,
                        'UserFirstName': this._data.getUserInfo().FirstName,
                        'UserLastName': this._data.getUserInfo().LastName,
                        'NotifyMe': notifyMe,
                        'NotifyLp': notifyLp,
                        'LpEmail': this._data.getLPInfo().Email,
                        'LpName': this._data.getLPInfo().Name,
                    };
                    return this._api.sendEmail(reqParam, this._config.getApiByName('SEND_EMAIL'));
                })
                .subscribe(res => {
                    observer.next(res.User && res.Lp);
                }, err => {
                    observer.next(false);
                }, () => {
                });
        });
    }

    //
    CreateStrainReview(): Observable<any> {
        return new Observable<any>(observer => {
            const tmpLp = this._data.getLPInfo();
            const tmpReview = this._data.getStrainReview();
            const tmpStrain = this._data.getSelectedStrain();
            const ttt = tmpStrain.Cannabinoids.map(each => each.Type.trim() + ':' + each.Value.toString()).toString();
            const reqParam = {
                'ReviewId': tmpReview.ReviewId === 'undefined' ? null : tmpReview.ReviewId,
                'LpId': tmpLp.Id,
                'LpName': tmpLp.Name,
                'ProductId': tmpStrain.ProductId,
                'ProductName': tmpStrain.ProductName,
                'ProductType': tmpStrain.ProductType.Type,
                'StrainType': tmpStrain.StrainType.Type,
                'Cannabinoids': tmpStrain.Cannabinoids.map(each => each.Type.trim() + ':' + each.Value.toString()).toString(),
                'Terpenes': tmpStrain.Terpenes.map(each => each.Type.trim() + ':' + each.Value.toString()).toString(),
                'BatchNumber': tmpReview.BatchNumber,
                'IntakeMethod': tmpReview.IntakeMethod,
                'MedicalConditionReview': tmpReview.MedicalConditionReview,
                'IntakeQuantity': tmpReview.IntakeQuantity,
                'PositiveEffect': tmpReview.PositiveEffect,
                'NegativeEffect': tmpReview.NegativeEffect
            };
            this._api.createStrainReview(reqParam, this._config.getApiByName('CREATE_STRAIN_REVIEW')).subscribe(res => {
                observer.next(res);
            });
        });
    }

    //
    CreateReview(): Observable<any> {
        return new Observable<any>(observer => {
            const tmpLp = this._data.getLPInfo();
            const tmpReview = this._data.getStrainReview();
            const tmpUser = this._data.getUserInfo();
            const reqParam = {
                'ReviewId': tmpReview.ReviewId === 'undefined' ? null : tmpReview.ReviewId,
                'LpId': tmpLp.Id,
                'LpName': tmpLp.Name,
                'UserFirstName': tmpUser.FirstName,
                'UserLastName': tmpUser.LastName,
                'UserEmail': tmpUser.Email,
                'AgeGroup': tmpReview.AgeGroup,
                'IntakeFrequency': tmpReview.IntakeFrequecy,
                'MedicalConditions': tmpReview.MedicalCondition,
                'SavedStrains': this._data.getSelectedStrainIdList(),
            };
            this._api.createReview(reqParam, this._config.getApiByName('CREATE_REVIEW')).subscribe(res => {
                observer.next(res);
            });
        });
    }

    StrainBusinessRtn(rtn: any): Observable<any> {
        return new Observable<any>(observer => {
            observer.next(rtn);
        });
    }
}
