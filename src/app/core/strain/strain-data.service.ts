import { Injectable } from '@angular/core';
//
import {
    StrainData,
    UserInfo, LPInfo, SearchFilter,
    AgeGroup, AgeGroupProxy,
    IntakeFrequecy, IntakeFrequecyProxy,
    MedicalCondition, MedicalConditionProxy,
    IntakeMethod, IntakeMethodProxy,
    PositiveEffect, PositiveEffectProxy,
    NegativeEffect, NegativeEffectProxy,
    StrainType, StrainTypeProxy,
    ProductType, ProductTypeProxy,
    ActivityType, ActivityTypeProxy,
    StrainReview,
    // StrainReviewProxy,
    CannabisProfile,
    Strain, StrainList, StrainInfo
} from './strain-data.model';
//
import { GlobalService } from '../global/global.service';
import { ConfigService } from '../config/config.service';

@Injectable()
export class StrainDataService {

    private strainData: StrainData; // = new StrainData();

    constructor(
        private _g: GlobalService
        , private _config: ConfigService
    ) {
        this.initStrainData();
    }
    //
    setUserInfo(data: UserInfo) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_USERINFO)) {
            this._g.setSession(this._config.CONFIG_DATA.SS_USERINFO, data, this._config.CONFIG_DATA.ENCRYPT);
        }
    }
    getUserInfo(): UserInfo {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_USERINFO)) {
            return this._g.getSession(this._config.CONFIG_DATA.SS_USERINFO, this._config.CONFIG_DATA.ENCRYPT);
        } else {
            return null;
        }
    }
    //
    setLPInfo(data: LPInfo) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_LPINFO)) {
            this._g.setSession(this._config.CONFIG_DATA.SS_LPINFO, data, this._config.CONFIG_DATA.ENCRYPT);
        }
    }
    getLPInfo(): LPInfo {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_LPINFO)) {
            return this._g.getSession(this._config.CONFIG_DATA.SS_LPINFO, this._config.CONFIG_DATA.ENCRYPT);
        } else {
            return null;
        }
    }
    //
    initStrainData() {
        if (this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)) {
            this.strainData = this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
        } else {
            this.strainData = new StrainData();
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, this.strainData, false);
        }
    }
    getStrainData(): StrainData {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
        } else {
            return null;
        }
    }
    // age group
    setAgeGroup(data: any) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const ageGroupList = new Array<AgeGroup>();
            data.map(each => {
                ageGroupList.push(AgeGroupProxy.Parse(JSON.stringify(each)));
            });
            tmp.ageGroup = ageGroupList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getAgeGroup(): AgeGroup[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).ageGroup;
        } else {
            return null;
        }
    }
    verifyAgeGroup(): boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            let rtn = false;
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.ageGroup.map(each => {
                if (each.Selected) {
                    rtn = true;
                }
            });
            return rtn;
        }
        return false;
    }
    //
    setIntakeFrequecy(data: IntakeFrequecy[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const intakeFrequecyList = new Array<IntakeFrequecy>();
            data.map(each => {
                intakeFrequecyList.push(IntakeFrequecyProxy.Parse(JSON.stringify(each)));
            });
            tmp.intakeFrequecy = intakeFrequecyList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getIntakeFrequecy(): IntakeFrequecy[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).intakeFrequecy;
        } else {
            return null;
        }
    }
    verifyIntakeFrequecy(): boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            let rtn = false;
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.intakeFrequecy.map(each => {
                if (each.Selected) {
                    rtn = true;
                }
            });
            return rtn;
        }
        return false;
    }
    //
    setMedicalCondition(data: MedicalCondition[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const medicalConditionList = new Array<MedicalCondition>();
            data.map(each => {
                medicalConditionList.push(MedicalConditionProxy.Parse(JSON.stringify(each)));
            });
            tmp.medicalCondition = medicalConditionList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getMedicalCondition(): MedicalCondition[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).medicalCondition;
        } else {
            return null;
        }
    }
    verifyMedicalCondition(from: number, to: number): boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            let rtn = false, counter = 0;
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.medicalCondition.map(each => {
                if (each.Selected) {
                    counter++;
                }
            });
            if (counter >= from && counter <= to) {
                rtn = true;
            }
            return rtn;
        }
        return false;
    }
    //
    setIntakeMethod(data: IntakeMethod[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const intakeMethodList = new Array<IntakeMethod>();
            data.map(each => {
                intakeMethodList.push(IntakeMethodProxy.Parse(JSON.stringify(each)));
            });
            tmp.intakeMethod = intakeMethodList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getIntakeMethod(): IntakeMethod[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).intakeMethod;
        } else {
            return null;
        }
    }
    //
    setPositiveEffect(data: PositiveEffect[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const positiveEffectList = new Array<PositiveEffect>();
            data.map(each => {
                positiveEffectList.push(PositiveEffectProxy.Parse(JSON.stringify(each)));
            });
            tmp.positiveEffect = positiveEffectList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getPositiveEffect(): PositiveEffect[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).positiveEffect;
        } else {
            return null;
        }
    }
    //
    setNegativeEffect(data: NegativeEffect[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const negativeEffectList = new Array<NegativeEffect>();
            data.map(each => {
                negativeEffectList.push(PositiveEffectProxy.Parse(JSON.stringify(each)));
            });
            tmp.negativeEffect = negativeEffectList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getNegativeEffect(): NegativeEffect[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).negativeEffect;
        } else {
            return null;
        }
    }
    //
    setStrainType(data: StrainType[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const strainTypeList = new Array<StrainType>();
            data.map(each => {
                strainTypeList.push(StrainTypeProxy.Parse(JSON.stringify(each)));
            });
            tmp.strainType = strainTypeList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getStrainType(): StrainType[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).strainType;
        } else {
            return null;
        }
    }
    //
    setProductType(data: ProductType[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const productTypeList = new Array<ProductType>();
            data.map(each => {
                productTypeList.push(ProductTypeProxy.Parse(JSON.stringify(each)));
            });
            tmp.productType = productTypeList;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getProductType(): ProductType[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).productType;
        } else {
            return null;
        }
    }
    //
    setSearchFilter(dataProductType: ProductType[], dataStrainType: StrainType[], dataActivity: ActivityType[], dataMedicalCondition: string, offset: number, next: number) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);

            if (dataMedicalCondition !== null) {
                tmp.searchFilter.medicalCondition = dataMedicalCondition;
            }

            if (dataProductType) {
                const productTypeList = new Array<ProductType>();
                dataProductType.map(each => {
                    productTypeList.push(ProductTypeProxy.Parse(JSON.stringify(each)));
                });
                tmp.searchFilter.productType = productTypeList;
            }

            if (dataStrainType) {
                const strainTypeList = new Array<StrainType>();
                dataStrainType.map(each => {
                    strainTypeList.push(StrainTypeProxy.Parse(JSON.stringify(each)));
                });
                tmp.searchFilter.strainType = strainTypeList;
            }

            if (dataActivity) {
                const activityTypeList = new Array<ActivityType>();
                dataActivity.map(each => {
                    activityTypeList.push(ActivityTypeProxy.Parse(JSON.stringify(each)));
                });
                tmp.searchFilter.activity = activityTypeList;
            }
            tmp.searchFilter.Offset = offset;
            tmp.searchFilter.Next = next;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    resetSearchFilter() {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.searchFilter.medicalCondition = null;
            tmp.searchFilter.Offset = 0;
            tmp.searchFilter.Next = this._config.CONFIG_DATA.PAGE_ITEMS;
            if (tmp.searchFilter.productType) {
                tmp.searchFilter.productType.forEach(each => each.Selected = true);
            }

            if (tmp.searchFilter.strainType) {
                tmp.searchFilter.strainType.forEach(each => each.Selected = true);
            }

            if (tmp.searchFilter.productType) {
                tmp.searchFilter.activity.forEach(each => each.Selected = true);
            }
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getSearchFilter(): SearchFilter {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).searchFilter;
        } else {
            return null;
        }
    }
    //
    setStrainReview(data: StrainReview) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.strainReview = data; // ProductTypeProxy.Parse(JSON.stringify(data));
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getStrainReview(): StrainReview {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).strainReview;
        } else {
            return null;
        }
    }
    //
    setCannabisProfile(data: CannabisProfile) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            const index = tmp.cannabisProfileList.indexOf(data);
            if (index > -1) {
                tmp.cannabisProfileList.splice(index, 1);
            }
            tmp.cannabisProfileList.push(data);
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getCannabisProfile(): CannabisProfile[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).cannabisProfileList;
        } else {
            return null;
        }
    }
    resetCannabisProfile(): Boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.cannabisProfileList = [];
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
            return true;
        }
        return false;
    }
    //
    setStrainList(data: StrainList) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            let index = -1;
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.strainList.map((each, i) => {
                if (each.MedicalCondition.Title === data.MedicalCondition.Title) {
                    index = i;
                }
            });
            if (index > -1) {
                tmp.strainList.splice(index, 1);
            }
            tmp.strainList.push(data);
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getStrainList(): StrainList[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).strainList;
        } else {
            return null;
        }
    }
    resetStrainList(): Boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.strainList = [];
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
            return true;
        }
        return false;
    }
    //
    setSelectedStrain(data: Strain) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA) && this.resetSelectedStrain()) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.selectedStrain = data;
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getSelectedStrain(): Strain {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).selectedStrain;
        } else {
            return null;
        }
    }
    resetSelectedStrain(): boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.selectedStrain = <Strain>{};
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
            return true;
        }
        return false;
    }
    //
    addSelectedStrainList(id: number) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            if (tmp.selectedStrainIdList.includes(id)) {
                tmp.selectedStrainIdList.push(id);
            }
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    removeSelectedStrainList(data: Strain) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA) && this.resetSelectedStrain()) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.selectedStrainList.map((each, index) => {
                if (each.ProductId === data.ProductId) {
                    tmp.selectedStrainList.splice(index, 1);
                }
            });
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getSelectedStrainList(): Strain[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).selectedStrainList;
        } else {
            return null;
        }
    }
    resetSelectedStrainList(): boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.selectedStrainList = [];
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
            return true;
        }
    }
    setSelectedStrainList(data: Strain) {
        data.ProductLogo = ''; // logo file too big to fill in sessionstorage
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            let available = false;
            tmp.selectedStrainList.map((each, index) => {
                if (each.ProductId === data.ProductId) {
                    tmp.selectedStrainList.splice(index, 1);
                    available = true;
                }
            });
            if (!available) {
                tmp.selectedStrainList.push(data);
            }
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    checkSelectedStrainList(data: Strain) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            let rtn = false;
            tmp.selectedStrainList.map(each => {
                if (each.ProductId === data.ProductId) {
                    rtn = true;
                }
            });
            return rtn;
        }
    }
    //
    addSelectedStrainIdList(data: Strain) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            if (tmp.selectedStrainList.indexOf(data) === -1) {
                tmp.selectedStrainList.push(data);
            }
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    removeSelectedStrainIdList(id: number) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA) && this.resetSelectedStrain()) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.selectedStrainIdList.map((each, index) => {
                if (each === id) {
                    tmp.selectedStrainIdList.splice(index, 1);
                }
            });
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    getSelectedStrainIdList(): number[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            return (<StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false)).selectedStrainIdList;
        } else {
            return null;
        }
    }
    resetSelectedStrainIdList(): boolean {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            tmp.selectedStrainIdList = [];
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
            return true;
        }
    }
    setSelectedStrainIdList(id: number) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            let available = false;
            tmp.selectedStrainIdList.map((each, index) => {
                if (each === id) {
                    tmp.selectedStrainIdList.splice(index, 1);
                    available = true;
                }
            });
            if (!available) {
                tmp.selectedStrainIdList.push(id);
            }
            this._g.setSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, tmp, false);
        }
    }
    checkSelectedStrainIdList(id: number) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_STRAIN_DATA)) {
            const tmp = <StrainData>this._g.getSession(this._config.CONFIG_DATA.SS_STRAIN_DATA, false);
            let rtn = false;
            tmp.selectedStrainIdList.map(each => {
                if (each === id) {
                    rtn = true;
                }
            });
            return rtn;
        }
    }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'MedicalConditionFilter'
})

export class MedicalConditionPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        if (!value) { return null; }
        if (!args) { return value; }
        return value.filter(each => {
            return each.description.toLowerCase().includes(args);
        });
    }
}
