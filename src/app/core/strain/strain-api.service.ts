import { Injectable } from '@angular/core';
//
import { HttpRequestService } from '../http/http-request.service';

@Injectable()
export class StrainApiService {
    constructor(
        private _http: HttpRequestService
    ) {
    }
    //
    getUserAuthorizationCode(data: string, url: string) {
        return this._http.postHttpRequestWithoutToken(data, url).map(res => res);
    }

    //
    getLicencedProducerInfo(data: any, url: string) {
        return this._http.postHttpRequestWithoutToken(data, url).map(res => res);
    }

    //
    getMasterInfoDetail(url: string) {
        return this._http.getHttpRequest(url).map(res => res);
    }

    getCannabisProfile(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }

    getStrainList(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }

    //
    getLicenseProducers(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }

    //
    getSuggestedDosage(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }
    //
    sendEmail(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }
    //
    createStrainReview(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }
    //
    createReview(data: any, url: string) {
        return this._http.postHttpRequest(data, url).map(res => res);
    }
}
