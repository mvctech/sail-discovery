import { GlobalFunction } from '../global/global-function.service';

export class StrainData {
    searchFilter = <SearchFilter>{};
    //
    ageGroup: AgeGroup[] = new Array<AgeGroup>();
    intakeFrequecy: IntakeFrequecy[] = new Array<IntakeFrequecy>();
    medicalCondition: MedicalCondition[] = new Array<MedicalCondition>();
    //
    // batchNumber = '';
    intakeMethod: IntakeMethod[] = new Array<IntakeMethod>();
    matchedMedicalCondition: MedicalCondition[] = new Array<MedicalCondition>();
    // amount = -1;
    positiveEffect: PositiveEffect[] = new Array<PositiveEffect>();
    negativeEffect: NegativeEffect[] = new Array<NegativeEffect>();
    //
    productType: ProductType[] = new Array<ProductType>();
    strainType: StrainType[] = new Array<StrainType>();
    //
    strainList: StrainList[] = new Array<StrainList>();
    // strainInfo = <StrainInfo>{};
    //
    cannabisProfileList: CannabisProfile[] = new Array<CannabisProfile>();
    // cannabisProfile = <CannabisProfile>{};
    //
    selectedStrain = <Strain>{};
    selectedStrainList: Strain[] = new Array<Strain>();
    selectedStrainIdList: number[] = new Array<number>();
    //
    strainReview = <StrainReview>{};
}
//
export interface UserInfo {
    Id: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Username: string;
    Role: string;
    Type: string;
}
export class UserInfoProxy {
    public readonly Id: number;
    public readonly FirstName: string;
    public readonly LastName: string;
    public readonly Email: string;
    public readonly Username: string;
    public readonly Role: string;
    public readonly Type: string;

    public static Parse(d: string): UserInfoProxy {
        return UserInfoProxy.Create(JSON.parse(d));
    }

    public static Create(d: any, field: string = 'root'): UserInfoProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.FirstName, false, field + '.FirstName');
        GlobalFunction.checkstring(d.LastName, false, field + '.LastName');
        GlobalFunction.checkstring(d.Username, false, field + '.Username');
        GlobalFunction.checkstring(d.Role, false, field + '.Role');
        GlobalFunction.checkstring(d.Type, false, field + '.Type');
        GlobalFunction.checkstring(d.Email, false, field + '.Email');
        return new UserInfoProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.FirstName = d.FirstName;
        this.LastName = d.LastName;
        this.Email = d.Email;
        this.Username = d.Username;
        this.Role = d.Role;
        this.Type = d.Type;
    }
}

//
export interface LPInfo {
    Id: number;
    Email: string;
    Logo: string;
    Name: String;
    Role: String;
}
export class LPInfoProxy {
    public readonly Id: number;
    public readonly Logo: string;
    public readonly Email: string;
    public readonly Name: string;
    public readonly Role: string;

    public static Parse(d: string): LPInfoProxy {
        return LPInfoProxy.Create(JSON.parse(d));
    }

    public static Create(d: any, field: string = 'root'): LPInfoProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Logo, false, field + '.Logo');
        GlobalFunction.checkstring(d.Name, false, field + '.Name');
        GlobalFunction.checkstring(d.Role, false, field + '.Role');
        GlobalFunction.checkstring(d.Email, false, field + '.Email');
        return new LPInfoProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Logo = d.Logo;
        this.Name = d.Name;
        this.Email = d.Email;
        this.Role = d.Role;
    }
}
//
export interface AgeGroup {
    Id: number;
    Title: string;
    Note: string;
    Selected: boolean;
}
export class AgeGroupProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Note: string;
    public readonly Selected: boolean;
    public static Parse(d: string): AgeGroupProxy {
        return AgeGroupProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): AgeGroupProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        //     checkNull(d.Note, field + '.Note');
        if (d.Selected === undefined) {
            d.Selected = false;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new AgeGroupProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Note = d.Note;
        this.Selected = d.Selected;
    }
}
//
export interface IntakeFrequecy {
    Id: number;
    Title: string;
    Note: string;
    Selected: boolean;
}
export class IntakeFrequecyProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Note: string;
    public readonly Selected: boolean;
    public static Parse(d: string): IntakeFrequecyProxy {
        return IntakeFrequecyProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): IntakeFrequecyProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        //     checkNull(d.Note, field + '.Note');
        if (d.Selected === undefined) {
            d.Selected = false;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new IntakeFrequecyProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Note = d.Note;
        this.Selected = d.Selected;
    }
}
//
export interface MedicalCondition {
    Id: number;
    Title: string;
    Note: string;
    Flag: boolean;
    Deleted: boolean;
    Selected: boolean;
}
export class MedicalConditionProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Note: string;
    public readonly Flag: boolean;
    public readonly Deleted: boolean;
    public readonly Selected: boolean;
    public static Parse(d: string): MedicalConditionProxy {
        return MedicalConditionProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): MedicalConditionProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        //     checkNull(d.Note, field + '.Note');
        if (d.Selected === undefined) {
            d.Selected = false;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new MedicalConditionProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Note = d.Note;
        this.Flag = d.Flag;
        this.Deleted = d.Deleted;
        this.Selected = d.Selected;
    }
}
//
export interface IntakeMethod {
    Id: number;
    Title: string;
    Selected: boolean;
}
export class IntakeMethodProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Selected: boolean;
    public static Parse(d: string): IntakeMethodProxy {
        return IntakeMethodProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): IntakeMethodProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        //     checkNull(d.Note, field + '.Note');
        if (d.Selected === undefined) {
            d.Selected = false;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new IntakeMethodProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Selected = d.Selected;
    }
}
//
export interface PositiveEffect {
    Id: number;
    Title: string;
    Selected: boolean;
}
export class PositiveEffectProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Selected: boolean;
    public static Parse(d: string): PositiveEffectProxy {
        return PositiveEffectProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): PositiveEffectProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        if (d.Selected === undefined) {
            d.Selected = false;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new PositiveEffectProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Selected = d.Selected;
    }
}
//
export interface NegativeEffect {
    Id: number;
    Title: string;
    Selected: boolean;
}
export class NegativeEffectProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Selected: boolean;
    public static Parse(d: string): NegativeEffectProxy {
        return NegativeEffectProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): NegativeEffectProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        if (d.Selected === undefined) {
            d.Selected = false;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new NegativeEffectProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Selected = d.Selected;
    }
}
//
export interface ProductType {
    Id: number;
    Type: string;
    Image: string;
    Color: string;
    Selected: boolean;
}
export class ProductTypeProxy {
    public readonly Id: number;
    public readonly Type: string;
    public readonly Image: string;
    public readonly Color: string;
    public readonly Selected: boolean;
    public static Parse(d: string): ProductTypeProxy {
        return ProductTypeProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): ProductTypeProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Type, false, field + '.Type');
        GlobalFunction.checkstring(d.Image, true, field + '.Image');
        GlobalFunction.checkstring(d.Color, true, field + '.Color');
        if (d.Selected === undefined) {
            d.Selected = true;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new ProductTypeProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Type = d.Type;
        this.Image = d.Image;
        this.Color = d.Color;
        this.Selected = d.Selected;
    }
}
//
export interface StrainType {
    Id: number;
    Type: string;
    Color: string;
    Selected: boolean;
}
export class StrainTypeProxy {
    public readonly Id: number;
    public readonly Type: string;
    public readonly Color: string;
    public readonly Selected: boolean;
    public static Parse(d: string): StrainTypeProxy {
        return StrainTypeProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): StrainTypeProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Type, false, field + '.Type');
        GlobalFunction.checkstring(d.Color, false, field + '.Color');
        if (d.Selected === undefined) {
            d.Selected = true;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new StrainTypeProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Type = d.Type;
        this.Color = d.Color;
        this.Selected = d.Selected;
    }
}
//
export interface ActivityType {
    Id: number;
    Title: string;
    Selected: boolean;
}
export class ActivityTypeProxy {
    public readonly Id: number;
    public readonly Title: string;
    public readonly Selected: boolean;
    public static Parse(d: string): ActivityTypeProxy {
        return ActivityTypeProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): ActivityTypeProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Title, false, field + '.Title');
        if (d.Selected === undefined) {
            d.Selected = true;
        }
        GlobalFunction.checkboolean(d.Selected, false, field + '.Selected');
        return new ActivityTypeProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Title = d.Title;
        this.Selected = d.Selected;
    }
}
//
export class SearchFilter {
    productType: ProductType[] = new Array<ProductType>();
    strainType: StrainType[] = new Array<StrainType>();
    activity: ActivityType[] = new Array<ActivityType>();
    medicalCondition: string;
    Offset: number;
    Next: number;
}
//
export interface StrainReview {
    ReviewId: string;
    //
    LPInfo: LPInfo;
    UserInfo: UserInfo;
    //
    AgeGroup: string;
    IntakeFrequecy: string;
    MedicalCondition: string;
    // Saved StrProductIDs
    RecommendedStrains: Strain[];
    // Last reviewed strain
    Strain: string;
    StrainId: number;
    //
    BatchNumber: string;
    IntakeMethod: string;
    MedicalConditionReview: string;
    IntakeQuantity: number;
    PositiveEffect: string;
    NegativeEffect: string;
}
// export class StrainReviewProxy {
//     public readonly AgeGroup: string;
//     public readonly IntakeFrequecy: string;
//     public readonly MedicalCondition: string;
//     //
//     public readonly Strain: string;
//     public readonly StrainId: number;
//     //
//     public readonly BatchNumber: number;
//     public readonly IntakeMethod: string;
//     public readonly MedicalConditionReview: string;
//     public readonly IntakeQuantity: number;
//     public readonly PositiveEffect: string;
//     public readonly NegativeEffect: string;
//     public static Parse(d: string): StrainReviewProxy {
//         return StrainReviewProxy.Create(JSON.parse(d));
//     }
//     public static Create(d: any, field: string = 'root'): StrainReviewProxy {
//         if (!field) {
//             GlobalFunction.obj = d;
//             field = 'root';
//         }
//         if (d === null || d === undefined) {
//             GlobalFunction.throwNull2NonNull(field, d);
//         } else if (typeof (d) !== 'object') {
//             GlobalFunction.throwNotObject(field, d, false);
//         } else if (Array.isArray(d)) {
//             GlobalFunction.throwIsArray(field, d, false);
//         }
//         GlobalFunction.checkstring(d.AgeGroup, false, field + '.AgeGroup');
//         GlobalFunction.checkstring(d.IntakeFrequecy, false, field + '.IntakeFrequecy');
//         GlobalFunction.checkstring(d.MedicalCondition, false, field + '.MedicalCondition');
//         //
//         GlobalFunction.checkstring(d.Strain, false, field + '.Strain');
//         GlobalFunction.checknumber(d.StrainId, false, field + '.StrainId');
//         //
//         GlobalFunction.checknumber(d.BatchNumber, true, field + '.BatchNumber');
//         GlobalFunction.checkstring(d.IntakeMethod, true, field + '.IntakeMethod');
//         GlobalFunction.checkstring(d.MedicalConditionReview, true, field + '.MedicalConditionReview');
//         GlobalFunction.checknumber(d.IntakeQuantity, true, field + '.IntakeQuantity');
//         GlobalFunction.checkstring(d.PositiveEffect, true, field + '.PositiveEffect');
//         GlobalFunction.checkstring(d.NegativeEffect, true, field + '.NegativeEffect');

//         return new StrainReviewProxy(d);
//     }
//     private constructor(d: any) {
//         this.AgeGroup = d.AgeGroup;
//         this.IntakeFrequecy = d.IntakeFrequecy;
//         this.StrainId = d.StrainId;
//         //
//         this.Strain = d.Strain;
//         this.BatchNumber = d.BatchNumber;
//         //
//         this.BatchNumber = d.BatchNumber;
//         this.IntakeMethod = d.Title;
//         this.MedicalConditionReview = d.MedicalConditionReview;
//         this.IntakeQuantity = d.Selected;
//         this.NegativeEffect = d.NegativeEffect;
//         this.PositiveEffect = d.PositiveEffect;
//     }
// }
//
export interface Cannabinoid {
    Value: number;
    Type: string;
}
export interface Terpene {
    Value: number;
    Type: string;
}
export interface LicenseProducer {
    LpId: number;
    LpName: string;
    LpLogo: string;
    Address: string;
    PostCode: string;
    City: string;
    State: string;
    Country: string;
    Phone: string;
}
export interface SuggestedUsage {
    ProductId: number;
    IntakeFrequecy: IntakeFrequecy;
    ActivityType: ActivityType;
    IntakeMode: string;
    DosageMin: number;
    DosageMax: number;
    Scale: string;
    Notes: string;
}
export interface StrainDosage {
    Min: number;
    Max: number;
    Scale: string;
}
export interface Strain {
    Id: number;
    ProductId: number;
    ProductName: string;
    ProductTypeId: number;
    ProductType: ProductType;
    StrainTypeId: number;
    StrainType: StrainType;
    ProductLogo: string;
    Cannabinoids: Cannabinoid[];
    Terpenes: Terpene[];
    LicenseProducerId: number;
    LicenseProducer: LicenseProducer;
    Activate: boolean;
    Relax: boolean;
    StrainDosage: StrainDosage;
}
export interface StrainId {
    ProductId: number;
    Activate: boolean;
    Relax: boolean;
}
export interface StrainList {
    MedicalCondition: MedicalCondition;
    // Strains: Strain[];
    StrainIds: StrainId[];
    Total: number;
}
export interface CannabisProfile {
    MedicalCondition: MedicalCondition;
    Cannabinoid: string[];
    Terpene: string[];
}
export class StrainInfo {
}


// Stores the currently-being-typechecked object for error messages.
// let obj: any = null;
// export class SAMPLEProxy {
//   public readonly Id: number;
//   public readonly Title: string;
//   public readonly Note: null;
//   public readonly Flag: boolean;
//   public readonly Deleted: boolean;
//   public static Parse(d: string): SAMPLEProxy {
//     return SAMPLEProxy.Create(JSON.parse(d));
//   }
//   public static Create(d: any, field: string = 'root'): SAMPLEProxy {
//     if (!field) {
//       GlobalFunction.obj = d;
//       field = 'root';
//     }
//     if (d === null || d === undefined) {
//       GlobalFunction.throwNull2NonNull(field, d);
//     } else if (typeof(d) !== 'object') {
//       GlobalFunction.throwNotObject(field, d, false);
//     } else if (Array.isArray(d)) {
//       GlobalFunction.throwIsArray(field, d, false);
//     }
//     GlobalFunction.checknumber(d.Id, false, field + '.Id');
//     GlobalFunction.checkstring(d.Title, false, field + '.Title');
//     checkNull(d.Note, field + '.Note');
//     if (d.Note === undefined) {
//       d.Note = null;
//     }
//     GlobalFunction.checkboolean(d.Flag, false, field + '.Flag');
//     GlobalFunction.checkboolean(d.Deleted, false, field + '.Deleted');
//     return new SAMPLEProxy(d);
//   }
//   private constructor(d: any) {
//     this.Id = d.Id;
//     this.Title = d.Title;
//     this.Note = d.Note;
//     this.Flag = d.Flag;
//     this.Deleted = d.Deleted;
//   }
// }
