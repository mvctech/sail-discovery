import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
//
import { ConfigService } from './config/config.service';
import { GlobalService } from './global/global.service';
import { GlobalErrorHandler } from './global/global-error-handler.service';
import { HttpRequestService } from './http/http-request.service';
import { WorkflowService } from './workflow/workflow.service';
import { WorkflowGuard } from './workflow/workflow-guard.service';
import { StrainDataService, MedicalConditionPipe } from './strain/strain-data.service';
import { StrainBusiness } from './strain/strain.business';
import { StrainApiService } from './strain/strain-api.service';

@NgModule({
    imports: [
        CommonModule
        , HttpModule
        , HttpClientModule
    ],
    declarations: [
        MedicalConditionPipe
    ],
    providers: [
        HttpClientModule
        , GlobalService
        , ConfigService
        , HttpRequestService
        , StrainDataService
        , StrainBusiness
        , StrainApiService
        , WorkflowService
        , { provide: ErrorHandler, useClass: GlobalErrorHandler }
    ],
    exports: [
        MedicalConditionPipe
    ]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                GlobalService
                , ConfigService
                , HttpRequestService
                , StrainDataService
                , StrainBusiness
                , StrainApiService
                , WorkflowService
                , WorkflowGuard
                , { provide: ErrorHandler, useClass: GlobalErrorHandler }
            ]
        };
    }
}
