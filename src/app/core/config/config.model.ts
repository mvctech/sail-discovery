import { GlobalFunction } from '../global/global-function.service';

export interface ApiEntity {
    Id: number;
    Name: string;
    Url: string;
}

export class ApiEntityProxy {
    public readonly Id: number;
    public readonly Name: string;
    public readonly Url: string;
    public static Parse(d: string): ApiEntityProxy {
        return ApiEntityProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): ApiEntityProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Name, false, field + '.Name');
        GlobalFunction.checkstring(d.Url, false, field + '.Url');
        return new ApiEntityProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Name = d.Name;
        this.Url = d.Url;
    }
}

export interface TxtGroupEntity {
    Lang: string;
    Data: TxtEntity[];
}

export interface TxtEntity {
    Id: number;
    Name: string;
    Txt: string;
    Desc: string;
}

export class TxtEntityProxy {
    public readonly Id: number;
    public readonly Name: string;
    public readonly Txt: string;
    public readonly Desc: string;
    public static Parse(d: string): TxtEntityProxy {
        return TxtEntityProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): TxtEntityProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Name, false, field + '.Name');
        GlobalFunction.checkstring(d.Txt, false, field + '.Txt');
        GlobalFunction.checkstring(d.Desc, true, field + '.Desc');
        return new TxtEntityProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Name = d.Name;
        this.Txt = d.Txt;
        this.Desc = d.Desc;
    }
}
