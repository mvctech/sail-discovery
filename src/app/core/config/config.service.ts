import {Injectable} from '@angular/core';
//
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
//
import {ApiEntity, ApiEntityProxy, TxtEntity, TxtEntityProxy, TxtGroupEntity} from './config.model';
import {apiData, txtGroupData} from './config.data';

class ConfigData {
  /* LOCAL ENVIRONMENT */
  // BASE_URL = 'http://localhost:10942';
  /* CANADIAN ENVIRONMENT */
  // BASE_URL = 'https://mvc-ca-api-stag.azurewebsites.net/';
  /* USA ENVIRONMENT */
  BASE_URL = 'https://mvc-us-api-uat.azurewebsites.net';
  // auth
  SF_ID = 'S20T17C03DD21';
  GRANT_PASS_TYPE = 'password';
  GRANT_TOKEN_TYPE = 'token';
  // GRANT_REFRESH_TOKEN_TYPE = 'refresh_token';
  // encrypt
  ENCRYPT = true;
  SALT = 'salt1234';
  // api
  APIS: ApiEntity[] = [];
  // gui text
  TXTGROUP: TxtGroupEntity[]; // = new Array<TxtGroupEntity>();
  TXTS: TxtEntity[] = [];
  // localStorage sessionStorage
  SS_WORKFLOW = 'wf';
  SS_USERINFO = 'ui';
  SS_LPINFO = 'li';
  SS_STRAIN_DATA = 'sd';
  //
  ROLE_LP = 'LicenseProducers';
  ROLE_USER = 'Guest';
  //
  PAGE_ITEMS = 10;

  constructor() {
    apiData.map(each => {
      this.APIS.push(ApiEntityProxy.Parse(JSON.stringify(each)));
    });
    this.TXTGROUP = txtGroupData;
  }
}

@Injectable()
export class ConfigService {
  CONFIG_DATA: ConfigData = new ConfigData();

  constructor() {
  }

  getBaseURL() {
    return this.CONFIG_DATA.BASE_URL;
  }

  getApiById(id: number) {
    return this.getReturnValue(this.CONFIG_DATA.APIS.filter(each => each.Id === id).map(each => each.Url));
  }

  getApiByName(name: string) {
    return this.getReturnValue(this.CONFIG_DATA.APIS.filter(each => each.Name === name).map(each => each.Url));
  }

  getTxtById(id: number) {
    return this.getReturnValue(this.CONFIG_DATA.TXTS.filter(each => each.Id === id).map(each => each.Txt));
  }

  getTxtByName(name: string) {
    return this.getReturnValue(this.CONFIG_DATA.TXTS.filter(each => each.Name === name).map(each => each.Txt));
  }

  LoadTxtDataByLang(lang: string) {
    const tmp = this.CONFIG_DATA.TXTGROUP.filter(each => each.Lang === lang);
    if (tmp.length === 1) {
      this.CONFIG_DATA.TXTGROUP.filter(each => each.Lang === lang)[0].Data.map(each => this.CONFIG_DATA.TXTS.push(TxtEntityProxy.Parse(JSON.stringify(each))));
    }
  }

  private getReturnValue(rtn: any) {
    if (rtn && rtn.length === 1) {
      return rtn.toString();
    } else {
      return '';
    }
  }
}
