import { ApiEntity, TxtEntity, TxtGroupEntity  } from './config.model';

export const apiData: ApiEntity[] = [
    { Id: 1, Name: 'API_TOKEN', Url: '/api/token' }
    , { Id: 2, Name: 'GET_LP_INFO', Url: '/api/guest/GetEmailByURL' }
    , { Id: 3, Name: 'GET_MASTER_INFO', Url: '/api/guest/GetMasterInfo' }
    //
    , { Id: 4, Name: 'GET_STRAIN_FINDER', Url: '/api/guest/StrainFinder' }
    , { Id: 5, Name: 'GET_STRAIN_LIST', Url: '/api/guest/GetStrainList' }
    // , { Id: 6, Name: 'GET_LP', Url: '/api/user/GetLicenseProducers' }
    , { Id: 7, Name: 'GET_SUGGESTED_DOSAGE', Url: '/api/guest/GetSuggestedDosage' }
    , { Id: 8, Name: 'SEND_EMAIL', Url: '/api/guest/SendEmailStrainDiscover' }
    , { Id: 9, Name: 'CREATE_REVIEW', Url: '/api/guest/CreateReview' }
    , { Id: 10, Name: 'CREATE_STRAIN_REVIEW', Url: '/api/guest/CreateStrainReview' }
];

const txtDataEn: TxtEntity[] = [
    { Id: 1, Name: 'Step1_sailstrains', Txt: 'Sail Strains, by Sail Cannabis', Desc: '' }
    , { Id: 2, Name: 'Step1_poweredby', Txt: 'Powered by Sail', Desc: '' }
    , { Id: 3, Name: 'Step1_introtext', Txt: 'Discover <b>ideal strains</b> to use for your medical condition, and find out how to use them effectively.', Desc: '' }
  , {Id: 4, Name: 'Step1_btntext', Txt: 'Get Started', Desc: ''}
];

const txtDataFr: TxtEntity[] = [
    { Id: 1, Name: 'Step1_sailstrains', Txt: 'Sail Strains, by Sail Cannabis', Desc: '' }
    , { Id: 2, Name: 'Step1_poweredby', Txt: 'Alimenté par Sail Cannabis', Desc: '' }
    , { Id: 3, Name: 'Step1_introtext', Txt: 'Discover <b>ideal strains</b> to use for your medical condition, and find out how to use them effectively.', Desc: '' }
    , { Id: 4, Name: 'Step1_btntext', Txt: 'Getting Started', Desc: '' }
];

const txtDataEs: TxtEntity[] = [
    { Id: 1, Name: 'Step1_sailstrains', Txt: 'Sail Strains, by Sail Cannabis', Desc: '' }
    , { Id: 2, Name: 'Step1_poweredby', Txt: 'Energizado por Sail Cannabis', Desc: '' }
    , { Id: 3, Name: 'Step1_introtext', Txt: 'Discover <b>ideal strains</b> to use for your medical condition, and find out how to use them effectively.', Desc: '' }
    , { Id: 4, Name: 'Step1_btntext', Txt: 'Getting Started', Desc: '' }
];

export const txtGroupData: TxtGroupEntity[] = [
    { Lang: 'en', Data: txtDataEn }
    , { Lang: 'fr', Data: txtDataFr }
    , { Lang: 'es', Data: txtDataEs }
];
