import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
//
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';
//
import { WorkflowData, StepsEntity, WorkFlowEntity, WorkFlowEntityProxy } from './workflow.model';
import { stepsData } from './workflow.data';
//
import { GlobalService } from '../global/global.service';
import { ConfigService } from '../config/config.service';

@Injectable()

export class WorkflowService {
    constructor(
        private _config: ConfigService
        , private _g: GlobalService
        , private _router: Router
    ) {
    }

    initSteps() {
        const tmp = <WorkflowData>{};
        const workflowList = new Array<WorkFlowEntity>();
        stepsData.map(each => {
            workflowList.push(WorkFlowEntityProxy.Parse(JSON.stringify(each)));
        });
        tmp.steps = workflowList;
        tmp.steptrace = new Array<StepsEntity>();
        this._g.setSession(this._config.CONFIG_DATA.SS_WORKFLOW, tmp, this._config.CONFIG_DATA.ENCRYPT);
    }
    // workflow
    private getWorkflow(): WorkFlowEntity[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_WORKFLOW)) {
            const ttt = <WorkflowData>this._g.getSession(this._config.CONFIG_DATA.SS_WORKFLOW, this._config.CONFIG_DATA.ENCRYPT);
            return (<WorkflowData>this._g.getSession(this._config.CONFIG_DATA.SS_WORKFLOW, this._config.CONFIG_DATA.ENCRYPT)).steps;
        } else {
            return null;
        }
    }
    private setWorkflow(data: any) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_WORKFLOW)) {
            const tmp = <WorkflowData>this._g.getSession(this._config.CONFIG_DATA.SS_WORKFLOW, this._config.CONFIG_DATA.ENCRYPT);
            tmp.steps = data;
            this._g.setSession(this._config.CONFIG_DATA.SS_WORKFLOW, tmp, this._config.CONFIG_DATA.ENCRYPT);
        }
    }
    // workflow steps
    resetSteps() {
        this.setStepsByPath('', 'step1');
        this.resetTrace();
    }
    private setStepsById(idFrom: Number, idTo: Number) {
        const tmpWorkflow = this.getWorkflow();
        if (!tmpWorkflow) {
            return;
        }
        tmpWorkflow.forEach(each => {
            if (each.Id === idFrom) {
                each.Valid = true;
            }
            if (each.Id === idTo) {
                each.Valid = false;
            }
        });
        this.setWorkflow(tmpWorkflow);
    }
    private setStepsByPath(urlFrom: string, urlTo: string) {
        const tmpWorkflow = this.getWorkflow();
        if (!tmpWorkflow) {
            return;
        }
        tmpWorkflow.forEach(each => {
            if (each.Url === urlFrom) {
                each.Valid = true;
            }
            if (each.Url === urlTo) {
                each.Valid = false;
            }
        });
        this.setWorkflow(tmpWorkflow);
    }
    // workflow trace
    private getWorkflowTrace(): StepsEntity[] {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_WORKFLOW)) {
            return (<WorkflowData>this._g.getSession(this._config.CONFIG_DATA.SS_WORKFLOW, this._config.CONFIG_DATA.ENCRYPT)).steptrace;
        } else {
            return null;
        }
    }
    private setWorkflowTrace(data: StepsEntity[]) {
        if (sessionStorage.getItem(this._config.CONFIG_DATA.SS_WORKFLOW)) {
            const tmp = <WorkflowData>this._g.getSession(this._config.CONFIG_DATA.SS_WORKFLOW, this._config.CONFIG_DATA.ENCRYPT);
            tmp.steptrace = data;
            this._g.setSession(this._config.CONFIG_DATA.SS_WORKFLOW, tmp, this._config.CONFIG_DATA.ENCRYPT);
        }
    }
    // workflow trace step
    private resetTrace() {
        this.setWorkflowTrace([]);
    }

    private addTraceById(id: number) {
        const tmp = this.getWorkflowTrace();
        tmp.push(stepsData.filter(each => each.Id === id).map(each => each)[0]);
        this.setWorkflowTrace(tmp);
    }

    private addTraceByPath(url: string) {
        const tmp = this.getWorkflowTrace();
        tmp.push(stepsData.filter(each => each.Url === url).map(each => each)[0]);
        this.setWorkflowTrace(tmp);
    }

    private removeTrace(): StepsEntity {
        const tmp = this.getWorkflowTrace();
        const rtn = tmp.pop();
        this.setWorkflowTrace(tmp);
        return rtn;
    }

    stepBack(urlFrom: string) {
        this.setStepsByPath(urlFrom, this.removeTrace().Url);
    }

    stepBackSkipByUrl(urlFrom: string, skipTo: string) {
        const tmp = this.getWorkflowTrace();
        let urlTo = <StepsEntity>{};
        for (let index = tmp.length, len = tmp.length; index < len; index--) {
            urlTo = this.removeTrace();
        }
        this.setStepsByPath(urlFrom, this.removeTrace().Url);
    }

    stepBackSkipByCounter(urlFrom: string, counter: number) {
        let urlTo = <StepsEntity>{};
        for (let index = 0; index < counter; index++) {
            urlTo = this.removeTrace();
        }
        this.setStepsByPath(urlFrom, this.removeTrace().Url);
    }

    stepForward(urlFrom: string, urlTo: string) {
        this.setStepsByPath(urlFrom, urlTo);
        this.addTraceByPath(urlFrom);
    }

    getFirstInvalidStep(): string {
        let redirectToStep = '';
        const tmpWorkflow = this.getWorkflow();
        if (!tmpWorkflow) {
            return redirectToStep;
        }
        if (tmpWorkflow && tmpWorkflow.length > 0) {
            tmpWorkflow.every(each => {
                redirectToStep = each.Url;
                return each.Valid;
            });
        }
        return redirectToStep;
    }
}
