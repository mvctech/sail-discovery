import { GlobalFunction } from '../global/global-function.service';

export class WorkflowData {
    steps: WorkFlowEntity[] = new Array<WorkFlowEntity>();
    steptrace: StepsEntity[] = new Array<StepsEntity>();
}

export interface StepsEntity {
    Id: number;
    Name: string;
    Url: string;
}

export interface WorkFlowEntity {
    Id: number;
    Name: string;
    Url: string;
    Valid: boolean;
}

export class WorkFlowEntityProxy {
    public readonly Id: number;
    public readonly Name: string;
    public readonly Url: string;
    public readonly Valid: boolean;
    public static Parse(d: string): WorkFlowEntityProxy {
        return WorkFlowEntityProxy.Create(JSON.parse(d));
    }
    public static Create(d: any, field: string = 'root'): WorkFlowEntityProxy {
        if (!field) {
            GlobalFunction.obj = d;
            field = 'root';
        }
        if (d === null || d === undefined) {
            GlobalFunction.throwNull2NonNull(field, d);
        } else if (typeof (d) !== 'object') {
            GlobalFunction.throwNotObject(field, d, false);
        } else if (Array.isArray(d)) {
            GlobalFunction.throwIsArray(field, d, false);
        }
        GlobalFunction.checknumber(d.Id, false, field + '.Id');
        GlobalFunction.checkstring(d.Name, false, field + '.Name');
        GlobalFunction.checkstring(d.Url, false, field + '.Url');
        if (d.Valid === undefined) {
            d.Valid = true;
        }
        GlobalFunction.checkboolean(d.Valid, false, field + '.Valid');
        return new WorkFlowEntityProxy(d);
    }
    private constructor(d: any) {
        this.Id = d.Id;
        this.Name = d.Name;
        this.Url = d.Url;
        this.Valid = d.Valid;
    }
}
