import { WorkFlowEntity, StepsEntity } from './workflow.model';

export const stepsData: StepsEntity[] = [
    { Id: 1, Name: 'step1', Url: 'step1' }
    , { Id: 2, Name: 'step2', Url: 'step2' }
    , { Id: 3, Name: 'step3', Url: 'step3' }
    , { Id: 4, Name: 'step4', Url: 'step4' }
    , { Id: 5, Name: 'step5', Url: 'step5' }
    , { Id: 6, Name: 'step6', Url: 'step6' }
    , { Id: 7, Name: 'step7', Url: 'step7' }
    , { Id: 8, Name: 'step8', Url: 'step8' }
    , { Id: 9, Name: 'step9', Url: 'step9' }
    , { Id: 10, Name: 'step10', Url: 'step10' }
    , { Id: 11, Name: 'step11', Url: 'step11' }
    , { Id: 12, Name: 'step12', Url: 'step12' }
    , { Id: 13, Name: 'step13', Url: 'step13' }
    , { Id: 14, Name: 'step14', Url: 'step14' }
    , { Id: 15, Name: 'step15', Url: 'step15' }
    , { Id: 16, Name: 'step16', Url: 'step16' }
    , { Id: 17, Name: 'step17', Url: 'step17' }
    , { Id: 18, Name: 'step18', Url: 'step18' }
    , { Id: 19, Name: 'step19', Url: 'step19' }
    , { Id: 20, Name: 'step20', Url: 'step20' }
    , { Id: 21, Name: 'step21', Url: 'step21' }
    , { Id: 22, Name: 'step22', Url: 'step22' }
    , { Id: 23, Name: 'step23', Url: 'step23' }
    , { Id: 24, Name: 'step24', Url: 'step24' }
    , { Id: 25, Name: 'step25', Url: 'step25' }
    , { Id: 26, Name: 'step26', Url: 'step26' }
];
