import { Injectable } from '@angular/core';
import {
    CanActivate, Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanLoad, Route
} from '@angular/router';

import { WorkflowService } from './workflow.service';

@Injectable()
export class WorkflowGuard implements CanActivate {
    constructor(
        private _router: Router
        , private _workflow: WorkflowService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const path = route.routeConfig.path;
        return this.verifyWorkFlow(path);
    }

    verifyWorkFlow(path): boolean {
        const firstPath = this._workflow.getFirstInvalidStep();
        if (path === firstPath) {
            return true;
        }
        if (firstPath && firstPath.length > 0) {
            const url = `/${firstPath}`;
            this._router.navigate([url]);
            return false;
        } else {
            this._router.navigate(['/']);
            return false;
        }
    }
}
