import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Step1Component} from './steps/step1/step1.component';
import {Step2Component} from './steps/step2/step2.component';
import {Step3Component} from './steps/step3/step3.component';
import {Step4Component} from './steps/step4/step4.component';
import {Step21Component} from './steps/step21/step21.component';
import {Step22Component} from './steps/step22/step22.component';
import {Step23Component} from './steps/step23/step23.component';
import {Step24Component} from './steps/step24/step24.component';
import {Step25Component} from './steps/step25/step25.component';
import {Step26Component} from './steps/step26/step26.component';
import {Step20Component} from './steps/step20/step20.component';
import {Step19Component} from './steps/step19/step19.component';
import {Step18Component} from './steps/step18/step18.component';
import {Step5Component} from './steps/step5/step5.component';
import {Step6Component} from './steps/step6/step6.component';
import {Step7Component} from './steps/step7/step7.component';
import {Step8Component} from './steps/step8/step8.component';
import {Step9Component} from './steps/step9/step9.component';
import {Step10Component} from './steps/step10/step10.component';
import {Step11Component} from './steps/step11/step11.component';
import {Step12Component} from './steps/step12/step12.component';
import {Step13Component} from './steps/step13/step13.component';
import {Step14Component} from './steps/step14/step14.component';
import {Step15Component} from './steps/step15/step15.component';
import {Step16Component} from './steps/step16/step16.component';
import {Step17Component} from './steps/step17/step17.component';
//
import { WorkflowGuard } from './core/workflow/workflow-guard.service';


const routes: Routes = [
  { path: '', component: Step1Component, canActivate: [WorkflowGuard] },
  { path: 'step1', component: Step1Component, canActivate: [WorkflowGuard] },
  { path: 'step2', component: Step2Component, canActivate: [WorkflowGuard] },
  { path: 'step3', component: Step3Component, canActivate: [WorkflowGuard] },
  { path: 'step4', component: Step4Component, canActivate: [WorkflowGuard] },
  { path: 'step5', component: Step5Component, canActivate: [WorkflowGuard] },
  { path: 'step6', component: Step6Component, canActivate: [WorkflowGuard] },
  { path: 'step7', component: Step7Component, canActivate: [WorkflowGuard] },
  {path: 'step8', component: Step8Component, canActivate: [WorkflowGuard]},
  {path: 'step9', component: Step9Component, canActivate: [WorkflowGuard]},
  { path: 'step10', component: Step10Component, canActivate: [WorkflowGuard] },
  { path: 'step11', component: Step11Component, canActivate: [WorkflowGuard] },
  { path: 'step12', component: Step12Component, canActivate: [WorkflowGuard] },
  { path: 'step13', component: Step13Component, canActivate: [WorkflowGuard] },
  { path: 'step14', component: Step14Component, canActivate: [WorkflowGuard] },
  { path: 'step15', component: Step15Component, canActivate: [WorkflowGuard] },
  { path: 'step16', component: Step16Component, canActivate: [WorkflowGuard] },
  { path: 'step17', component: Step17Component, canActivate: [WorkflowGuard] },
  { path: 'step18', component: Step18Component, canActivate: [WorkflowGuard] },
  { path: 'step19', component: Step19Component, canActivate: [WorkflowGuard] },
  { path: 'step20', component: Step20Component, canActivate: [WorkflowGuard] },
  { path: 'step21', component: Step21Component, canActivate: [WorkflowGuard] },
  { path: 'step22', component: Step22Component, canActivate: [WorkflowGuard] },
  { path: 'step23', component: Step23Component, canActivate: [WorkflowGuard] },
  { path: 'step24', component: Step24Component, canActivate: [WorkflowGuard] },
  { path: 'step25', component: Step25Component, canActivate: [WorkflowGuard] },
  { path: 'step26', component: Step26Component, canActivate: [WorkflowGuard] },
  { path: '**', component: Step1Component, canActivate: [WorkflowGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [WorkflowGuard]
})

export class AppRoutingModule {
}
