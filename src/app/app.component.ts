import { Component } from '@angular/core';
//
import { ConfigService } from './core/config/config.service';
import { StrainBusiness } from './core/strain/strain.business';
import { WorkflowService } from './core/workflow/workflow.service';

@Component({
  selector: 'sailstrains-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private _config: ConfigService
    , private _sb: StrainBusiness
    , private _workflow: WorkflowService
  ) {
    // this._sb.strainInit();
    this._config.LoadTxtDataByLang('en');
  }
}
