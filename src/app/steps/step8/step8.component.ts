import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { CannabisProfile } from '../../core/strain/strain-data.model';

@Component({
  selector: 'sailstrains-step8',
  templateUrl: './step8.component.html',
  styleUrls: ['./step8.component.scss']
})
export class Step8Component implements OnInit {
  text1 = 'Recommended Cannabinoids';
  text2 = 'What are cannabinoids?';
  text3 = 'Suggested Terpenes';
  text4 = 'What are terpenes?';
  b1selected: boolean;
  b1selectedClass: string;

  conditions = {
    language: 'en',
    selector_name: 'screen8_',
    options: [
    ]
  };

  cannabioids = {
    language: 'en',
    selector_name: 'screen8_',
    options: [
    ]
  };

  terpeneses = {
    language: 'en',
    selector_name: 'screen8_',
    options: [
    ]
  };
  conditionFilter = '';
  tmpA: any;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) {
    this.b1selected = false;
  }

  ngOnInit() {
    this.conditions.options = [];
    const tmp = this._data.getCannabisProfile();
    this.tmpA = tmp;
    tmp.map(each => {
      this.conditions.options.push({ id: each.MedicalCondition.Id, title: each.MedicalCondition.Title });
    });
    this.conditionFilter = this.conditions.options[0].title;
    this.setGUIData();
  }

  private setGUIData() {
    this.cannabioids.options = [];
    this.terpeneses.options = [];
    const tmp = this._data.getCannabisProfile();
    if (tmp.length > 0) {
      const tmpProfile = tmp.filter(each => each.MedicalCondition.Title === this.conditionFilter);
      if (tmpProfile.length === 1) {
        tmpProfile[0].Cannabinoid.splice(0, 3).map(each => {
          this.cannabioids.options.push({ title: each });
        });
        tmpProfile[0].Terpene.splice(0, 3).map(each => {
          this.terpeneses.options.push({ title: each });
        });
      }
    }
  }

  onChange(value: any) {
    this.conditionFilter = value;
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step8');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goCannabinoid(event: any) {
    this._workflow.stepForward('step8', 'step9');
  }

  goTerpene(event: any) {
    this._workflow.stepForward('step8', 'step10');
  }
}
