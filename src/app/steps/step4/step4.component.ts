import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { IntakeFrequecy } from '../../core/strain/strain-data.model';

@Component({
  selector: 'sailstrains-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})
export class Step4Component implements OnInit {
  temp: any;
  single_selection = {
    language: 'en',
    selector_name: 'screen4_experience',
    options: [
    ]
  };

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpIntakeFrequecy = this._data.getIntakeFrequecy();
    if (tmpIntakeFrequecy.length === 0) {
      return;
      // throw new Error('getIntakeFrequecy error');
    }
    tmpIntakeFrequecy.map(each => {
      each.Selected = (each.Title === tmpStrainReviewData.IntakeFrequecy);
    });
    this.single_selection.options = [];
    tmpIntakeFrequecy.map(each => {
      this.single_selection.options.push({ id: each.Id, name: each.Id.toString(), description: each.Title, note: each.Note, selected: each.Selected });
    });
  }

  onElementSelection(id: any) {
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        tmp.IntakeFrequecy = each.description;
      }
    });
    this._data.setStrainReview(tmp);
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step4');
  }

  goNext(event: any) {
    if (this.verifyIntakeFrequecy()) {
      this._workflow.stepForward('step4', 'step5');
    }
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  verifyIntakeFrequecy(): boolean {
    const tmp = this._data.getStrainReview();
    if (tmp.IntakeFrequecy && tmp.IntakeFrequecy.length > 0) {
      return true;
    }
    return false;
  }
}
