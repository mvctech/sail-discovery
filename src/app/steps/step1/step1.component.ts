import { Component, OnInit } from '@angular/core';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { StrainBusiness } from '../../core/strain/strain.business';
import { StrainReview } from '../../core/strain/strain-data.model';

@Component({
  selector: 'sailstrains-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})

export class Step1Component implements OnInit {
  sailstrains = this._config.getTxtByName('Step1_sailstrains'); // 'Sail Strains, by Sail Cannabis';
  poweredby = this._config.getTxtByName('Step1_poweredby'); // 'Powered by Sail Cannabis';
  introtext = this._config.getTxtByName('Step1_introtext'); // 'Discover ideal strains to use for your medical condition, and find out how to use them effectively.';
  btntext = this._config.getTxtByName('Step1_btntext');
  enableButton = false;
  constructor(
    private _config: ConfigService
    , private _workflow: WorkflowService
    , private _sb: StrainBusiness
    , private _data: StrainDataService
  ) {
  }

  ngOnInit() {
    this._data.initStrainData();
    this._sb.StrainInit().subscribe(res => {
      if (res) {
          this.enableButton = true;
      }
    });
    this._workflow.initSteps();
    this._workflow.stepForward('step1', 'step1');
  }

  goNext(event: any) {
    if (this.enableButton) {
      this._workflow.stepForward('step1', 'step2');
    }
  }
}
