import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step16',
  templateUrl: './step16.component.html',
  styleUrls: ['./step16.component.scss']
})

export class Step16Component implements OnInit {
  BatchNumber: string;
  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) {
  }

  ngOnInit() {
    this.setGUIData();
  }

  goPrev(event: any) {
    if (this.saveData()) {
      this._workflow.stepBack('step16');
    }
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goNext(event: any) {
    if (this.saveData()) {
      this._workflow.stepForward('step16', 'step17');
    }
  }

  private setGUIData() {
    const tmp = this._data.getStrainReview();
    if (!tmp) {
      throw new Error('getStrainReview error');
    }
    this.BatchNumber = tmp.BatchNumber;
  }

  private saveData(): boolean {
    const tmp = this._data.getStrainReview();
    tmp.BatchNumber = this.BatchNumber;
    this._data.setStrainReview(tmp);
    return true;
  }
}
