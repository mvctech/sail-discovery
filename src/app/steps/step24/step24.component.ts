import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';

@Component({
  selector: 'sailstrains-step24',
  templateUrl: './step24.component.html',
  styleUrls: ['./step24.component.scss']
})
export class Step24Component implements OnInit {
  header_text: String = 'You Haven’t Saved Any Strains Yet';
  terms_conditions1: String = 'Are you sure you want to email this information to yourself with out specific strain selections?';
  terms_conditions2: String = 'You can review and save some of your recommended strains before sending if you’d like.';

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
  ) {
  }

  ngOnInit() {
  }

}
