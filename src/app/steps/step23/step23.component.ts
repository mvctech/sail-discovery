import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { StrainBusiness } from '../../core/strain/strain.business';

@Component({
  selector: 'sailstrains-step23',
  templateUrl: './step23.component.html',
  styleUrls: ['./step23.component.scss']
})
export class Step23Component implements OnInit {

  strains = {
    language: 'en',
    selector_name: 'screen11_',
    options: [
    ]
  };
  strainList: any;
  loadingSpinner: boolean;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
    , private _sb: StrainBusiness
  ) { }

  ngOnInit() {
    this.clearReviewData();
    this.setGUIData();
  }

  private clearReviewData(): boolean {
    const tmp = this._data.getStrainReview();
    delete tmp.Strain;
    delete tmp.StrainId;
    delete tmp.BatchNumber;
    delete tmp.IntakeMethod;
    delete tmp.MedicalConditionReview;
    delete tmp.IntakeQuantity;
    delete tmp.PositiveEffect;
    delete tmp.NegativeEffect;
    this._data.setStrainReview(tmp);
    return true;
  }

  private setGUIData() {
    this.loadingSpinner = true;
    this._sb.GetSavedStrainList().subscribe(res => {
      this.loadingSpinner = false;
      if (res === null) {
        return;
      }
      if (res.length > 0) {
        this.strainList = res;
        res.map(each => {
          this.strains.options.push({
            id: each.Id,
            productid: each.ProductId,
            title: each.ProductName,
            type: each.StrainType.Type,
            THC: each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'THC').length === 1 ? each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'THC')[0].Value : 0,
            CBD: each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'CBD').length === 1 ? each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'CBD')[0].Value : 0,
            img: each.ProductType.Image, // each.ProductLogo.length > 0 ? 'data:image/jpeg;base64,' + each.ProductLogo : each.ProductType.Image,
            productType: each.ProductType.Type,
            productColor: each.ProductType.Color,
          });
        });
      }
    });
  }


  goPrev(event: any) {
    this._workflow.stepBack('step23');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goStrain(event: any) {
    this._data.setSelectedStrain(this.strainList.filter(each => each.ProductId === event)[0]);
    this._workflow.stepForward('step23', 'step14');
  }
}
