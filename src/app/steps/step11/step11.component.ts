import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
import { StrainBusiness } from '../../core/strain/strain.business';
import { StrainDataService } from '../../core/strain/strain-data.service';
import { SearchFilter, StrainReview } from '../../core/strain/strain-data.model';
import { StrainList } from '../../core/strain/strain-data.model';

@Component({
  selector: 'sailstrains-step11',
  templateUrl: './step11.component.html',
  styleUrls: ['./step11.component.scss']
})
export class Step11Component implements OnInit {

  results = { filtered: 0, total: 0 };
  // resultString = 'Showing {0} of {1} Strains.';
  // resultString = 'Showing {0} strains';
  conditionFilter = '';
  conditions = {
    language: 'en',
    selector_name: 'screen11_',
    options: [
    ]
  };

  strains = {
    language: 'en',
    selector_name: 'screen11_',
    options: [
    ]
  };

  strainList: any;

  page = {
    from: 0,
    to: 0,
    total: 0
  };

  loadingSpinner: boolean;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
    , private _sb: StrainBusiness
  ) {
  }

  ngOnInit() {
    this.conditions.options = [];
    const tmp = this._data.getStrainList();
    tmp.map(each => {
      this.conditions.options.push({ id: each.MedicalCondition.Id, title: each.MedicalCondition.Title });
    });
    if (this._data.getSearchFilter().medicalCondition && this._data.getSearchFilter().medicalCondition.length > 0) {
      this.conditionFilter = this._data.getSearchFilter().medicalCondition;
    } else if (this.conditions.options.length > 0) {
      this._data.setSearchFilter(null, null, null, this.conditions.options[0].title, 0, this._config.CONFIG_DATA.PAGE_ITEMS);
      this.conditionFilter = this._data.getSearchFilter().medicalCondition;
    } else {
      this._data.setSearchFilter(null, null, null, '', 0, this._config.CONFIG_DATA.PAGE_ITEMS);
      this.conditionFilter = '';
    }
    this.clearReviewData();
    this.setGUIData();
  }

  private clearReviewData(): boolean {
    const tmp = this._data.getStrainReview();
    delete tmp.Strain;
    delete tmp.StrainId;
    delete tmp.BatchNumber;
    delete tmp.IntakeMethod;
    delete tmp.MedicalConditionReview;
    delete tmp.IntakeQuantity;
    delete tmp.PositiveEffect;
    delete tmp.NegativeEffect;
    this._data.setStrainReview(tmp);
    return true;
  }

  private setGUIData() {
    this.loadingSpinner = true;
    const tmpFilter = this._data.getSearchFilter();
    const tmpProductTypeFilter = tmpFilter.productType.filter(each => each.Selected).map(each => each.Type).toString();
    const tmpStrainTypeFilter = tmpFilter.strainType.filter(each => each.Selected).map(each => each.Type).toString();
    const tmpActivityFilter = tmpFilter.activity.filter(each => each.Selected).map(each => each.Title).toString();
    const tmp = this._data.getStrainList();
    let tmpStrainList = this._data.getStrainList().filter(each => each.MedicalCondition.Title === this.conditionFilter);
    this.strains.options = [];
    if (tmpStrainList.length === 1) {
      const tmpMedicalCondition = this._data.getMedicalCondition().filter(each => each.Title === this.conditionFilter)[0];
      this._sb.GetStrainList(tmpMedicalCondition, this._data.getSearchFilter().Offset, this._data.getSearchFilter().Next).subscribe(res => {
        this.loadingSpinner = false;
        if (res === null) {
          return;
        }
        tmpStrainList = this._data.getStrainList().filter(each => each.MedicalCondition.Title === this.conditionFilter);
        this.page.total = tmpStrainList[0].Total;
        this.page.from = this._data.getSearchFilter().Offset + 1;
        this.page.to = (this.page.from + this._config.CONFIG_DATA.PAGE_ITEMS -1 < this.page.total) ? this.page.from + this._config.CONFIG_DATA.PAGE_ITEMS -1 : this.page.total;
        this.strainList = res;
        this.results.total = res.length;
        res.map(each => {
          let selected = false;
          if ((each.ProductType && tmpProductTypeFilter.indexOf(each.ProductType.Type) > -1) && (each.StrainType && tmpStrainTypeFilter.indexOf(each.StrainType.Type) > -1)) {
            if ((tmpActivityFilter.indexOf('ACTIVE') > -1) && each.Activate) {
              selected = true;
            } else if ((tmpActivityFilter.indexOf('RELAXED') > -1) && each.Relax) {
              selected = true;
            }
          }
          if (selected) {
            this.strains.options.push({
              id: each.Id,
              productid: each.ProductId,
              title: each.ProductName,
              type: each.StrainType.Type,
              THC: each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'THC').length === 1 ? each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'THC')[0].Value : 0,
              CBD: each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'CBD').length === 1 ? each.Cannabinoids.filter(data => data.Type.trim().toUpperCase() === 'CBD')[0].Value : 0,
              img: each.ProductType.Image, // each.ProductLogo.length > 0 ? 'data:image/jpeg;base64,' + each.ProductLogo : each.ProductType.Image,
              productType: each.ProductType.Type,
              productColor: each.ProductType.Color,
            });
          }
        });
        this.results.filtered = this.strains.options.length;
      });
    }
  }

  onChange(value: any) {
    const tmp = this._data.getSearchFilter();
    this._data.setSearchFilter(null, null, null, value, tmp.Offset, tmp.Next);
    this.conditionFilter = value;
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step11');
    this._data.resetSearchFilter();
  }

  goNext(event: any) {
    this._workflow.stepForward('step11', 'step6');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goFilter(event: any) {
    this._workflow.stepForward('step11', 'step13');
    this._router.navigate(['/step13']);
  }

  goStrain(id: any) {
    const tmp = this._data.getStrainList().filter(each => each.MedicalCondition.Title === this.conditionFilter)[0];
    this._data.setSelectedStrain(this.strainList.filter(each => each.ProductId === id)[0]);
    this._workflow.stepForward('step11', 'step14');
  }

  pagePrev() {
    if (this.page.from >= this._config.CONFIG_DATA.PAGE_ITEMS) {
      this.page.from -= this._config.CONFIG_DATA.PAGE_ITEMS;
      this.page.to = this.page.from + this._config.CONFIG_DATA.PAGE_ITEMS;
      this._data.setSearchFilter(null, null, null, null, this.page.from - 1, this._config.CONFIG_DATA.PAGE_ITEMS);
      this.setGUIData();
    }
  }
  pageNext() {
    const offset = this.page.from + this._config.CONFIG_DATA.PAGE_ITEMS -1;
    if (offset < this.page.total) {
      this.page.from += this._config.CONFIG_DATA.PAGE_ITEMS;
      this.page.to = (offset < this.page.total) ? offset : this.page.total;
      this._data.setSearchFilter(null, null, null, null, offset, this._config.CONFIG_DATA.PAGE_ITEMS);
      this.setGUIData();
    }
  }
}
