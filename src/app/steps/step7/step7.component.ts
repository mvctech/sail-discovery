import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
@Component({
  selector: 'sailstrains-step7',
  templateUrl: './step7.component.html',
  styleUrls: ['./step7.component.scss']
})
export class Step7Component implements OnInit {

  textValues = {
    language: 'en',
    selector_name: 'screen7_',
    options: [
      {
        id: '1',
        title: 'Your Cannabis Profile',
        body: 'Lorem Ipsum is simply dummy text of the printing and' +
          ' typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
      },
      {
        id: '2',
        title: 'Your Recommended Strains',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
          ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
          ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
          ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      },
      {
        id: '3',
        title: 'Your Saved Strains',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
          ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
          ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
          ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      }
    ]
  };

  selectedStrains = 0;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    const tmpSelectedStrainList = this._data.getSelectedStrainIdList();
    this.selectedStrains = tmpSelectedStrainList.length;
  }

  goPrev(event: any) {
    this._workflow.stepBack('step7');
  }

  goNext(event: any) {
    this._workflow.stepForward('step7', 'step25');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goStrainProfile() {
    this._workflow.stepForward('step7', 'step8');
  }

  goStrains() {
    this._workflow.stepForward('step7', 'step11');
  }

  goStrainSelected() {
    this._workflow.stepForward('step7', 'step23');
  }

}
