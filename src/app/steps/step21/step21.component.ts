import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { StrainBusiness } from '../../core/strain/strain.business';

@Component({
  selector: 'sailstrains-step21',
  templateUrl: './step21.component.html',
  styleUrls: ['./step21.component.scss']
})
export class Step21Component implements OnInit {
  single_selection = {
    language: 'en',
    selector_name: 'screen5_',
    options: [
    ]
  };

  canMoveOn = false;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
    , private _sb: StrainBusiness
  ) { }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpNegativeEffect = this._data.getNegativeEffect();
    if (tmpNegativeEffect.length === 0) {
      throw new Error('getNegativeEffect error');
    }

    if (tmpStrainReviewData.NegativeEffect) {
      const tmpArray = tmpStrainReviewData.NegativeEffect.split(',');
      tmpNegativeEffect.map(each => {
        tmpArray.map(data => {
          if (each.Title.trim().toLowerCase() === data.trim().toLocaleLowerCase()) {
            each.Selected = true;
          }
        });
      });
    }

    this.single_selection.options = [];
    tmpNegativeEffect.map(each => {
      this.single_selection.options.push({ id: each.Id, name: each.Id.toString(), description: each.Title, selected: each.Selected });
    });
  }

  onElementSelection(id: any) {
    this.canMoveOn = true;
    let dirty = false;
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        each.selected = !each.selected;
        dirty = true;
      }
    });
    if (dirty) {
      tmp.NegativeEffect = this.single_selection.options.filter(each => each.selected === true).map(each => each.description.trim()).toString();
      this._data.setStrainReview(tmp);
    }
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step21');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }
  goNext(event: any) {
    this._sb.CreateStrainReview().subscribe( res => {
      if (res.length > 0 && res !== '00000000-0000-0000-0000-000000000000') {
        const tmp = this._data.getStrainReview();
        tmp.ReviewId = res;
        this._data.setStrainReview(tmp);
        this._workflow.stepForward('step21', 'step22');
        this._router.navigate(['/step22']);
      } else {

      }
    });
  }
}
