import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
//
import {ConfigService} from '../../core/config/config.service';
import {WorkflowService} from '../../core/workflow/workflow.service';
//
import {StrainDataService} from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step17',
  templateUrl: './step17.component.html',
  styleUrls: ['./step17.component.scss']
})
export class Step17Component implements OnInit {
  single_selection = {
    language: 'en',
    selector_name: 'screen17_',
    options: []
  };
  IntakeMethod: string;
  canMoveOn = false;

  constructor(private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService) {
  }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpIntakeMethod = this._data.getIntakeMethod();
    if (tmpIntakeMethod.length === 0) {
      throw new Error('getIntakeMethod error');
    }
    tmpIntakeMethod.map(each => {
      each.Selected = (each.Title === tmpStrainReviewData.IntakeMethod);
    });
    this.single_selection.options = [];
    tmpIntakeMethod.map(each => {
      this.single_selection.options.push({
        id: each.Id,
        name: each.Id.toString(),
        description: each.Title,
        selected: each.Selected
      });
    });
  }

  private onElementSelection(id: any) {
    this.canMoveOn = true;
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        if (tmp.IntakeMethod === each.description) {
          tmp.IntakeMethod = '';
        } else {
          tmp.IntakeMethod = each.description;
        }
      }
    });
    this._data.setStrainReview(tmp);
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step17');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goNext(event: any) {
    this._workflow.stepForward('step17', 'step18');
  }
}
