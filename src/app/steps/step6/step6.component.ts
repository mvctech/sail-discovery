import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { StrainBusiness } from '../../core/strain/strain.business';
import { MedicalCondition } from '../../core/strain/strain-data.model';
//
@Component({
  selector: 'sailstrains-step6',
  templateUrl: './step6.component.html',
  styleUrls: ['./step6.component.scss']
})
export class Step6Component implements OnInit {

  single_selection = {
    language: 'en',
    selector_name: 'screen6_',
    options: [
      {
        id: '1',
        name: 'value1',
        description: 'Daily'
      },
      {
        id: '2',
        name: 'value2',
        description: 'More Than Once a Month'
      },
      {
        id: '3',
        name: 'value3',
        description: 'Infrequently / Never'
      }
    ]
  };
  // text1 = '';
  // text2 = '';

  result = 0;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
    , private _sb: StrainBusiness
  ) { }

  ngOnInit() {
    this.result = 0;
    this._data.resetCannabisProfile();
    this._data.resetStrainList();
    const tmpMedicalConditionList = [];
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpMedicalCondition = this._data.getMedicalCondition();
    if (tmpMedicalCondition.length === 0) {
      return;
    }
    tmpMedicalCondition.map(each => {
      if (tmpStrainReviewData.MedicalCondition && tmpStrainReviewData.MedicalCondition.indexOf(each.Title.trim()) > -1) {
        tmpMedicalConditionList.push(each);
      }
    });

    tmpMedicalConditionList.map(each => {
      const tmpMC = <MedicalCondition>{};
      tmpMC.Id = each.Id;
      tmpMC.Title = each.Title;
      this._sb.GetStrainProfile(tmpMC, 0, this._config.CONFIG_DATA.PAGE_ITEMS).subscribe(res => {
        if (res) {
          this.result = 1;
        } else {
          this.result = (this.result === 1) ?  1: 2;
        }
      });
    });
  }

  goPrev(event: any) {
    this._workflow.stepBack('step6');
  }

  goNext(event: any) {
    if (this.result === 1) {
      this._workflow.stepForward('step6', 'step7');
    }
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }
}
