import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step19',
  templateUrl: './step19.component.html',
  styleUrls: ['./step19.component.scss']
})
export class Step19Component implements OnInit {
  IntakeQuantity: number;
  canMoveOn = false;
  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmp = this._data.getStrainReview();
    if (!tmp) {
      throw new Error('getStrainReview error');
    }
    this.IntakeQuantity = tmp.IntakeQuantity;
  }

  private saveData(): boolean {
    if (typeof (Number(this.IntakeQuantity)) === 'number' && Number(this.IntakeQuantity) > 0) {
      const tmp = this._data.getStrainReview();
      tmp.IntakeQuantity = Number(this.IntakeQuantity);
      this._data.setStrainReview(tmp);
      return true;
    }
    return false;
  }

  goPrev(event: any) {
    this._workflow.stepBack('step19');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goNext(event: any) {
    if (this.saveData()) {
      this._workflow.stepForward('step19', 'step20');
    }
  }
}
