import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
//
import {ConfigService} from '../../core/config/config.service';
import {WorkflowService} from '../../core/workflow/workflow.service';
//
import {StrainDataService} from '../../core/strain/strain-data.service';
import {SearchFilter} from '../../core/strain/strain-data.model';

@Component({
  selector: 'sailstrains-step13',
  templateUrl: './step13.component.html',
  styleUrls: ['./step13.component.scss']
})
export class Step13Component implements OnInit {
  header_text: String = 'Strain Filters';

  products = {
    language: 'en',
    selector_name: 'screen13_',
    text1: 'PRODUCT TYPE',
    options: []
  };
  strains = {
    language: 'en',
    selector_name: 'screen13_',
    text2: 'STRAIN TYPE',
    options: []
  };
  activities = {
    language: 'en',
    selector_name: 'screen13_',
    text3: 'ACTIVITY',
    options: []
  };

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService) {
  }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmp = <SearchFilter>this._data.getSearchFilter();
    if (!tmp) {
      throw new Error('getSearchFilter error');
    }

    tmp.productType.map(each => {
      this.products.options.push({ id: each.Id, title: each.Id.toString(), type: each.Type, selected: each.Selected });
    });
    tmp.strainType.map(each => {
      this.strains.options.push({ id: each.Id, title: each.Id.toString(), type: each.Type, selected: each.Selected });
    });
    tmp.activity.map(each => {
      this.activities.options.push({ id: each.Id, title: each.Id.toString(), type: each.Title, selected: each.Selected });
    });
  }

  goReset(event: any) {
    this.products.options.forEach(each => each.selected = true);
    this.strains.options.forEach(each => each.selected = true);
    this.activities.options.forEach(each => each.selected = true);
  }

  goSave(event: any) {
    const tmp = this._data.getSearchFilter();
    tmp.productType.map(each => {
      this.products.options.forEach(data => {
        if (each.Id === data.id) {
          each.Selected = data.selected;
        }
      });
    });
    tmp.strainType.map(each => {
      this.strains.options.forEach(data => {
        if (each.Id === data.id) {
          each.Selected = data.selected;
        }
      });
    });
    tmp.activity.map(each => {
      this.activities.options.forEach(data => {
        if (each.Id === data.id) {
          each.Selected = data.selected;
        }
      });
    });
    this._data.setSearchFilter(tmp.productType, tmp.strainType, tmp.activity, null, 0, tmp.Next );
    this._workflow.stepBack('step13');
  }

  overlayClicked(event: any) {
    const check = (a) => event.target.classList.contains(a);
    if (check('component-root') || check('closeIcon')) {
      this._workflow.stepBack('step13');
      this._router.navigate(['/step11']);
    }
  }

  goCancel(event: any) {
    this._workflow.stepBack('step13');
  }
}
