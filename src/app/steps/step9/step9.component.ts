import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
//
import {ConfigService} from '../../core/config/config.service';
import {WorkflowService} from '../../core/workflow/workflow.service';

@Component({
  selector: 'sailstrains-step9',
  templateUrl: './step9.component.html',
  styleUrls: ['./step9.component.scss']
})
export class Step9Component implements OnInit {
  header_text: String = 'What Are Cannabinoids?';
  terms_conditions1: String = 'This is an Agreement between you' +
    ' (oruser,your) and MVC Technologies Inc. (or MVC, we,us,our). It' +
    ' governs your use of the website, www.sailcannabis.co, and portal,' +
    ' sailportal.co, (thePortal) (collectively, theSite Services) offered by MVC.';
  terms_conditions2: String = 'This Agreement incorporates by reference the' +
    ' MVC Privacy Policy. MVC reserves the right, at its sole discretion,' +
    ' to modify, discontinue or terminate the Site Services or to modify' +
    ' these Terms, at any time and without prior notice. If we modify these' +
    ' Terms we will post the modification via the Site or Portal or provide' +
    ' you with notice of the modification. By continuing to access or use' +
    ' the Services after we have posted a modification via the Site or' +
    ' Portal or have provided you with notice of a modification, you agree' +
    ' to be bound by the modified Terms. If the modified Terms are not' +
    ' acceptable to you, you agree to immediately stop using the Site Services.';
  terms_conditions3: String = 'This Agreement incorporates by reference the' +
    ' MVC Privacy Policy. MVC reserves the right, at its sole discretion,' +
    ' to modify, discontinue or terminate the Site Services or to modify' +
    ' these Terms, at any time and without prior notice. If we modify these' +
    ' Terms we will post the modification via the Site or Portal or provide' +
    ' you with notice of the modification. By continuing to access or use' +
    ' the Services after we have posted a modification via the Site or' +
    ' Portal or have provided you with notice of a modification, you agree' +
    ' to be bound by the modified Terms. If the modified Terms are not' +
    ' acceptable to you, you agree to immediately stop using the Site Services.';

  constructor(private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService) {
  }

  ngOnInit() {
  }

  goPrev(event: any) {
    this._workflow.stepBack('step9');
  }

  overlayClicked(event: any) {
    const check = (a) => event.target.classList.contains(a);
    if (check('component-root') || check('closeIcon')) {
      this._workflow.stepBack('step9');
      this._router.navigate(['/step8']);
    }
  }
}
