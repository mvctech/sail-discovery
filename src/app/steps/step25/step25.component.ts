import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { Observable } from 'rxjs/Observable';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
import { StrainBusiness } from '../../core/strain/strain.business';
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step25',
  templateUrl: './step25.component.html',
  styleUrls: ['./step25.component.scss']
})

export class Step25Component implements OnInit {

  data = {
    language: 'en',
    selector_name: 'screen25_',
    options: [
      // {
      //   id: '1',
      //   title: 'Tahoe OG',
      //   type: 'Sativa',
      //   THC: '3.1%',
      //   CBD: '15.4%',
      //   body: 'Lorem Ipsum is simply dummy text of the printing and' +
      //   ' typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
      // },
    ],
    fName: '',
    lName: '',
    email: '',
    notifyLp: false,
    notifyMe: false,
  };
  showNotification: boolean;
  loadingSpinner: boolean;
  notifyLpOption:boolean;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _sb: StrainBusiness
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    this.showNotification = false;
    this.loadingSpinner = false;
    this.notifyLpOption = this._data.getSelectedStrainIdList().length > 0;
  }

  goPrev(event: any) {
    this._workflow.stepBack('step25');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  sendEmail(event: any) {
    this.loadingSpinner = true;
    const tmp = this._data.getUserInfo();
    tmp.FirstName = this.data.fName;
    tmp.LastName = this.data.lName;
    tmp.Email = this.data.email;
    if (tmp.Email.length === 0) {
      return;
    }
    this._data.setUserInfo(tmp);

    this._sb.CreateReview()
      .mergeMap(res => {
        if (res.length > 0 && res !== '00000000-0000-0000-0000-000000000000') {
          const tmpReview = this._data.getStrainReview();
          tmpReview.ReviewId = res;
          this._data.setStrainReview(tmpReview);
          return this._sb.SendEmail(this.data.notifyMe, this.data.notifyLp);
        } else {
          return this._sb.StrainBusinessRtn(false);
        }
      })
      .subscribe(
        res => {
          this.loadingSpinner = false;
          if (res) {
            this._workflow.stepForward('step25', 'step26');
            this._router.navigate(['/step26']);
          } else {
            this.showNotification = true;
          }
        }
        , err => {
          this.loadingSpinner = false;
          this.showNotification = true;
        }
        , () => {
        }
      );
  }

  validateEmail(): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.data.email).toLowerCase());
  }

}
