import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step15',
  templateUrl: './step15.component.html',
  styleUrls: ['./step15.component.scss']
})
export class Step15Component implements OnInit {
  strainName = '';
  strainNote = '';
  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) {
  }

  ngOnInit() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpIntakeFrequecy = this._data.getIntakeFrequecy();
    if (tmpIntakeFrequecy.length === 0) {
      throw new Error('getIntakeFrequecy error');
    }
    const tmp = tmpIntakeFrequecy.filter(each => each.Title === tmpStrainReviewData.IntakeFrequecy);
    if (tmp.length === 1) {
      this.strainNote = tmp[0].Note;
    }
    const tmpSelectedStrain = this._data.getSelectedStrain();
    this.strainName = 'Dosage Guidelines and Conumption Methods for ' + tmpSelectedStrain.ProductName;
  }

  onSave() {
  }

  goPrev(event: any) {
    this._workflow.stepBack('step15');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }
}
