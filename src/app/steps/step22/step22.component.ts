import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step22',
  templateUrl: './step22.component.html',
  styleUrls: ['./step22.component.scss']
})
export class Step22Component implements OnInit {

  text1 = 'This information will help patients like yourself find a strain' +
    ' that works well for them.';

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
  }

  goPrev(event: any) {
    this._workflow.stepBack('step22');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }
  goStrain(event: any) {
    this._workflow.stepBackSkipByCounter('step22', 5);
  }
}
