import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';

@Component({
  selector: 'sailstrains-step12',
  templateUrl: './step12.component.html',
  styleUrls: ['./step12.component.scss']
})
export class Step12Component implements OnInit {

  results = 'Showing 0 Strains.';

  conditions = {
    language: 'en',
    selector_name: 'screen11_',
    options: [
      {
        id: '1',
        title: 'Anxiety when relaxing',
        body: 'Lorem Ipsum is simply dummy text of the printing and' +
        ' typesetting industry. Lorem Ipsum has been the industry\'s standard ' +
        'dummy text ever since the 1500s, when an unknown printer took a galley of ' +
        'type and scrambled it to make a type specimen book. It has survived not only five ' +
        'centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'
      },
      {
        id: '2',
        title: 'Anxiety when relaxing',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
        ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
        ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
        ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      },
      {
        id: '3',
        title: 'Anxiety when relaxing',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
        ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
        ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
        ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      }
    ]
  };

  strains = {
    language: 'en',
    selector_name: 'screen11_',
    options: [
      {
        id: '1',
        title: 'Tahoe OG',
        type: 'Sativa',
        THC: '3.1%',
        CBD: '15.4%',
        body: 'Lorem Ipsum is simply dummy text of the printing and' +
        ' typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
      },
      {
        id: '2',
        title: 'Kosher Kush',
        type: 'Sativa',
        THC: '5.3%',
        CBD: '10.2%',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
        ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
        ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
        ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      },
      {
        id: '3',
        title: 'New one',
        type: 'Sativa',
        THC: '5.3%',
        CBD: '10.2%',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
        ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
        ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
        ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      },
      {
        id: '4',
        title: 'Another one',
        type: 'Sativa',
        THC: '5.3%',
        CBD: '10.2%',
        body: 'Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.' +
        ' Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem' +
        ' Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum' +
        ' is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum. Lorem Ipsum is Lorem Ipsum and Lorem Ipsum is Lorem Ipsum.'
      }
    ]
  };

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
  ) {
  }

  ngOnInit() {
  }

}
