import {Component, EventEmitter, Output} from '@angular/core';
import {Router} from '@angular/router';
import {ConfigService} from '../../core/config/config.service';
import {WorkflowService} from '../../core/workflow/workflow.service';

@Component({
  selector: 'sailstrains-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component {
  header_text: String = 'Terms & Privacy Policy';
  terms_conditions_title1: String = '1. YOUR ACCEPTANCE OF THIS AGREEMENT';
  terms_conditions1: String = 'This is an Agreement between you (oruser,your) and MVC Technologies Inc. (orMVC, we,us,our). It governs your use of the website, www.sailcannabis.co, and portal, sailportal.co, (thePortal) (collectively, theSite Services) offered by MVC.';
  terms_conditions2: String = 'YOU ACKNOWLEDGE AND AGREE THAT, BY CLICKING ON THEAGREE BUTTON, OR ACCESSING OR USING THE SITE SERVICES, OR BY DOWNLOADING OR POSTING ANY CONTENT FROM OR THROUGH THE SITE SERVICES, YOU ARE INDICATING THAT YOU HAVE READ, AND UNDERSTAND AND AGREE TO BE BOUND BY, THE TERMS AND CONDITIONS (THETERMS) CONTAINED BELOW, WHETHER OR NOT YOU HAVE REGISTERED VIA THE SITE OR PORTAL. YOU REPRESENT AND WARRANT THAT YOU HAVE THE LEGAL AUTHORITY TO AGREE TO AND ACCEPT THIS AGREEMENT ON BEHALF OF YOURSELF. YOU HAVE NO RIGHT TO ACCESS OR USE THE SERVICES UNLESS YOU AGREE TO THESE TERMS.';
  terms_conditions3: String = 'PLEASE READ THIS DOCUMENT CAREFULLY.';
  terms_conditions4: String = 'This Agreement incorporates by reference the MVC Privacy Policy. MVC reserves the right, at its sole discretion, to modify, discontinue or terminate the Site Services or to modify these Terms, at any time and without prior notice. If we modify these Terms we will post the modification via the Site or Portal or provide you with notice of the modification. By continuing to access or use the Services after we have posted a modification via the Site or Portal or have provided you with notice of a modification, you agree to be bound by the modified Terms. If the modified Terms are not acceptable to you, you agree to immediately stop using the Site Services.';

  progress_indicator = 0;
  progress_indicator_max = 0;

  constructor(private _router: Router,
              private _config: ConfigService,
              private _workflow: WorkflowService) {
    this.progress_indicator = 0;
    this.progress_indicator_max = 100;
  }

  goPrev(event: any) {
    this._workflow.stepBack('step2');
  }

  goNext(event: any) {
    this._workflow.stepForward('step2', 'step3');
  }

  overlayClicked(event: any) {
    const check = (a) => event.target.classList.contains(a);
    if (check('component-root') || check('closeIcon')) {
      this._workflow.resetSteps();
      this._router.navigate(['/']);
    }
  }
}
