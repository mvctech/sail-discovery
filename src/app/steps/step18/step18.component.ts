import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step18',
  templateUrl: './step18.component.html',
  styleUrls: ['./step18.component.scss']
})
export class Step18Component implements OnInit {
  single_selection = {
    language: 'en',
    selector_name: 'screen5_',
    options: [
    ]
  };

  // disabled: boolean;
  subtitle = '5 max. If your condition isn’t listed here, please select the closest related condition.';
  alert = 'Please select an option.';
  strFilter;
  canMoveOn = false;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpMedicalCondition = this._data.getMedicalCondition();
    if (tmpMedicalCondition.length === 0) {
      throw new Error('getMedicalCondition error');
    }

    if (tmpStrainReviewData.MedicalConditionReview) {
      const tmpArray = tmpStrainReviewData.MedicalConditionReview.split(',');
      tmpMedicalCondition.map(each => {
        tmpArray.map(data => {
          if (each.Title.trim().toLowerCase() === data.trim().toLocaleLowerCase()) {
            each.Selected = true;
          }
        });
      });
    }

    this.single_selection.options = [];
    tmpMedicalCondition.map(each => {
      this.single_selection.options.push({ id: each.Id, name: each.Id.toString(), description: each.Title, note: each.Note, flag: each.Flag, deleted: each.Deleted, selected: each.Selected });
    });
  }

  private onElementSelection(id: any) {
    this.canMoveOn = true;
    let dirty = false;
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        each.selected = !each.selected;
        dirty = true;
      }
    });
    if (dirty) {
      tmp.MedicalConditionReview = this.single_selection.options.filter(each => each.selected === true).map(each => each.description.trim()).toString();
      this._data.setStrainReview(tmp);
    }
    this.setGUIData();
  }
  goPrev(event: any) {
    this._workflow.stepBack('step18');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goNext(event: any) {
    this._workflow.stepForward('step18', 'step19');
  }

  private verifyMedicalCondition(): number {
    return this.single_selection.options.filter(each => each.selected === true).length;
  }
}
