import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/do';
import * as Chart from 'chart.js';
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
import { StrainDataService } from '../../core/strain/strain-data.service';
import { StrainBusiness } from '../../core/strain/strain.business';

@Component({
  selector: 'sailstrains-step14',
  templateUrl: './step14.component.html',
  styleUrls: ['./step14.component.scss']
})
export class Step14Component implements OnInit, AfterViewInit {
  text1 = 'text1';
  text2 = 'text2';
  text3 = 'Recommended Consumption and Dosing';
  text4 = '';
  text5 = 'View Details';
  saved = false;
  firstLoad: boolean;
  hideElement: boolean;

  strains = {
    language: 'en',
    selector_name: 'screen11_',
    options: {
      ProductName: '',
      Activate: false,
      Relax: false
    }
  };
  btnRemove = false;
  LP_Address = '';
  LP_Logo: any;

  // chart
  canvasCannabinoids: any;
  ctxCannabinoids: any;
  canvasTerpenes: any;
  ctxTerpenes: any;
  chartData = { Cannabinoids: [], Terpenes: [] };
  ActiveOrRelax = 1;
  cannabinoidsSize = -1;
  terpenesSize = -1;

  constructor(private _config: ConfigService,
    private _workflow: WorkflowService,
    private _data: StrainDataService,
    private _sb: StrainBusiness,
    private _ds: DomSanitizer) {
  }

  ngOnInit() {
    this.initStrainData();
    this.getLPInfo();
    this.getSuggesstedDosage();
  }

  ngAfterViewInit() {
    if (this.chartData.Cannabinoids.length > 0) { this.setCannabinoidsChart(); }
    if (this.chartData.Terpenes.length > 0) { this.setTerpenesChart(); }
  }

  private setCannabinoidsChart() {
    this.canvasCannabinoids = document.getElementById('chartCannabinoids');
    this.ctxCannabinoids = this.canvasCannabinoids.getContext('2d');
    const chartCannabinoids = new Chart(this.ctxCannabinoids, {
      type: 'doughnut',
      data: {
        labels: this.chartData.Cannabinoids
          .map(each => each.Type.trim()),
        datasets: [{
          data: this.chartData.Cannabinoids.map(each => {
            return each.Value;
          }), // ,
          backgroundColor: [
            'rgba(123,127,240,1)',
            'rgba(48,210,212,1)',
            'rgba(56,178,234,1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        display: false,
        cutoutPercentage: 80,
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontColor: 'rgb(255, 99, 132)'
          }
        }
      }
    });
  }

  private setTerpenesChart() {
    this.canvasTerpenes = document.getElementById('chartTerpenes');
    this.ctxTerpenes = this.canvasTerpenes.getContext('2d');
    const chartTerpenes = new Chart(this.ctxTerpenes, {
      type: 'doughnut',
      data: {
        labels: this.chartData.Terpenes.map(each => each.Type.trim()),
        datasets: [{
          data: this.chartData.Terpenes.map(each => each.Value), // ,
          backgroundColor: [
            'rgba(123,127,240,1)',
            'rgba(48,210,212,1)',
            'rgba(56,178,234,1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        responsive: true,
        display: false,
        cutoutPercentage: 80,
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontColor: 'rgb(255, 99, 132)'
          }
        }
      }
    });
  }

  onActivaty($event) {
    this.ActiveOrRelax = 1;
    this.getSuggesstedDosage();
  }

  onRelax($event) {
    this.ActiveOrRelax = 2;
    this.getSuggesstedDosage();
  }

  private getSuggesstedDosage() {
    const tmpIntakeFrequency = this._data.getIntakeFrequecy();
    const tmpExperienceId = tmpIntakeFrequency.filter(each => each.Title === this._data.getStrainReview().IntakeFrequecy).map(each => each.Id)[0];
    this._sb.GetSuggestedDosage(this._data.getSelectedStrain().ProductId, tmpExperienceId, this.ActiveOrRelax)
      .subscribe(res => {
        if (res) {
          const tmpF = this._data.getIntakeFrequecy();
          const tmpSelectedStrain = this._data.getSelectedStrain();
          if (tmpSelectedStrain.StrainDosage.Min === tmpSelectedStrain.StrainDosage.Max) {
            this.text4 = tmpSelectedStrain.StrainDosage.Min + ' '
              + (tmpSelectedStrain.StrainDosage.Scale === null ? '' : tmpSelectedStrain.StrainDosage.Scale)
              + ' DAILY';
          } else {
            this.text4 = tmpSelectedStrain.StrainDosage.Min + ' - '
              + tmpSelectedStrain.StrainDosage.Max + ' '
              + (tmpSelectedStrain.StrainDosage.Scale === null ? '' : tmpSelectedStrain.StrainDosage.Scale)
              + ' DAILY';
          }
        } else {
          this.text4 = 'N/A';
        }
      });
  }

  onSaveOrRemove() {
    this._data.setSelectedStrainIdList(this._data.getSelectedStrain().ProductId);
    this.btnRemove = this._data.checkSelectedStrainIdList(this._data.getSelectedStrain().ProductId);
  }

  onReview() {
    this.firstLoad = !this.firstLoad;
  }

  private initStrainData() {
    const tmp = this._data.getSelectedStrain();
    this.ActiveOrRelax = tmp.Activate ? 1 : 2;
    this.text1 = tmp.ProductType.Type;
    this.text2 = tmp.StrainType.Type;
    this.strains.options = tmp;
    this.btnRemove = this._data.checkSelectedStrainIdList(this._data.getSelectedStrain().ProductId);
    this.chartData.Cannabinoids = tmp.Cannabinoids.sort((a, b) => {
      if (a.Value < b.Value) { return 1; } else if (a.Value > b.Value) { return -1; } else { return 0; }
    }).map(each => each).slice(0, 3);
    this.chartData.Terpenes = tmp.Terpenes.sort((a, b) => {
      if (a.Value < b.Value) { return 1; } else if (a.Value > b.Value) { return -1; } else { return 0; }
    }).map(each => each).slice(0, 3);
  }

  private getLPInfo() {
    const tmpSelectedStrain = this._data.getSelectedStrain();
    if (tmpSelectedStrain.LicenseProducer) {
      this.LP_Address = tmpSelectedStrain.LicenseProducer.LpName + ':<br/>' +
        tmpSelectedStrain.LicenseProducer.Address + ', ' +
        tmpSelectedStrain.LicenseProducer.City + ', ' +
        tmpSelectedStrain.LicenseProducer.State + ' ' +
        tmpSelectedStrain.LicenseProducer.Country + ' ' +
        tmpSelectedStrain.LicenseProducer.PostCode + '<br/>' +
        tmpSelectedStrain.LicenseProducer.Phone;
      this.LP_Logo = this._ds.bypassSecurityTrustUrl(tmpSelectedStrain.LicenseProducer.LpLogo);
    }
  }

  goPrev(event: any) {
    this._workflow.stepBack('step14');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  goDetail(event: any) {
    this._workflow.stepForward('step14', 'step15');
  }

  goReview(event: any) {
    this._workflow.stepForward('step14', 'step17');
  }
}
