import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';

@Component({
  selector: 'sailstrains-step26',
  templateUrl: './step26.component.html',
  styleUrls: ['./step26.component.scss']
})
export class Step26Component implements OnInit {

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
  ) {
  }

  ngOnInit() {
  }

  goContinue(event: any) {
    // this._workflow.stepBack('step26');
    this._workflow.stepBackSkipByCounter('step26', 1);
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

}
