import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { AgeGroup, AgeGroupProxy } from '../../core/strain/strain-data.model';
@Component({
  selector: 'sailstrains-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {
  single_selection = {
    language: 'en',
    selector_name: 'screen3_age',
    options: [
    ]
  };
  tmp: any;
  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpAgeGroup = this._data.getAgeGroup();
    if (tmpAgeGroup.length === 0) {
      return;
      // throw new Error('getAgeGroup error');
    }
    tmpAgeGroup.map(each => {
      each.Selected = (each.Title === tmpStrainReviewData.AgeGroup);
    });
    this.single_selection.options = [];
    tmpAgeGroup.map(each => {
      this.single_selection.options.push({ id: each.Id, name: each.Id.toString(), description: each.Title, note: each.Note, selected: each.Selected });
    });
  }

  onElementSelection(id: any) {
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        tmp.AgeGroup = each.description;
      }
    });
    this._data.setStrainReview(tmp);
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step3');
  }

  goNext(event: any) {
    if (this.verifyAgeGroup()) {
      this._workflow.stepForward('step3', 'step4');
    }
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  verifyAgeGroup(): boolean {
    const tmp = this._data.getStrainReview();
    if (tmp.AgeGroup && tmp.AgeGroup.length > 0) {
      return true;
    }
    return false;
  }
}
