import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';
import { MedicalCondition } from '../../core/strain/strain-data.model';
//
import { StrainBusiness } from '../../core/strain/strain.business';

@Component({
  selector: 'sailstrains-step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.scss']
})
export class Step5Component implements OnInit {
  single_selection = {
    language: 'en',
    selector_name: 'screen5_',
    options: []
  };

  subtitle = '5 max. If your condition isn’t listed here, please select the closest related condition.';
  alert = 'Please select an option.';
  strFilter;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
    , private _sb: StrainBusiness
  ) { }

  ngOnInit() {
    this._data.setSearchFilter(null, null, null, '', 0 , this._config.CONFIG_DATA.PAGE_ITEMS);
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpMedicalCondition = this._data.getMedicalCondition();
    if (tmpMedicalCondition.length === 0) {
      return;
      // throw new Error('getMedicalCondition error');
    }
    if (tmpStrainReviewData.MedicalCondition) {
      const tmpArray = tmpStrainReviewData.MedicalCondition.split(',');
      tmpMedicalCondition.map(each => {
        tmpArray.map(data => {
          if (each.Title.trim().toLowerCase() === data.trim().toLowerCase()) {
            each.Selected = true;
          }
        });
      });
    }
    this.single_selection.options = [];
    tmpMedicalCondition.map(each => {
      this.single_selection.options.push({ id: each.Id, name: each.Id.toString(), description: each.Title, note: each.Note, flag: each.Flag, deleted: each.Deleted, selected: each.Selected });
    });
  }

  private onElementSelection(id: any) {
    let dirty = false;
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        if (each.selected) {
          each.selected = !each.selected;
          dirty = true;
        } else {
          if (this.verifyMedicalCondition() < 5) {
            each.selected = !each.selected;
            dirty = true;
          }
        }
      }
    });
    if (dirty) {
      tmp.MedicalCondition = this.single_selection.options.filter(each => each.selected === true).map(each => each.description).toString();
      this._data.setStrainReview(tmp);
    }
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step5');
  }

  goNext(event: any) {
    const tmp = this.verifyMedicalCondition();
    if (tmp >= 1 && tmp <= 5) {
      this._workflow.stepForward('step5', 'step6');
    }
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }

  verifyMedicalCondition(): number {
    return this.single_selection.options.filter(each => each.selected === true).length;
  }
}
