import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//
import { ConfigService } from '../../core/config/config.service';
import { WorkflowService } from '../../core/workflow/workflow.service';
//
import { StrainDataService } from '../../core/strain/strain-data.service';

@Component({
  selector: 'sailstrains-step20',
  templateUrl: './step20.component.html',
  styleUrls: ['./step20.component.scss']
})
export class Step20Component implements OnInit {
  single_selection = {
    language: 'en',
    selector_name: 'screen5_',
    options: [
    ]
  };

  canMoveOn = false;

  constructor(
    private _router: Router
    , private _config: ConfigService
    , private _workflow: WorkflowService
    , private _data: StrainDataService
  ) { }

  ngOnInit() {
    this.setGUIData();
  }

  private setGUIData() {
    const tmpStrainReviewData = this._data.getStrainReview();
    const tmpPositiveEffect = this._data.getPositiveEffect();
    if (tmpPositiveEffect.length === 0) {
      throw new Error('getPositiveEffect error');
    }

    if (tmpStrainReviewData.PositiveEffect) {
      const tmpArray = tmpStrainReviewData.PositiveEffect.split(',');
      tmpPositiveEffect.map(each => {
        tmpArray.map(data => {
          if (each.Title.trim().toLowerCase() === data.trim().toLocaleLowerCase()) {
            each.Selected = true;
          }
        });
      });
    }

    this.single_selection.options = [];
    tmpPositiveEffect.map(each => {
      this.single_selection.options.push({ id: each.Id, name: each.Id.toString(), description: each.Title, selected: each.Selected });
    });
  }

  onElementSelection(id: any) {
    this.canMoveOn = true;
    let dirty = false;
    const tmp = this._data.getStrainReview();
    this.single_selection.options.map(each => {
      if (each.id === id) {
        each.selected = !each.selected;
        dirty = true;
      }
    });
    if (dirty) {
      tmp.PositiveEffect = this.single_selection.options.filter(each => each.selected === true).map(each => each.description.trim()).toString();
      this._data.setStrainReview(tmp);
    }
    this.setGUIData();
  }

  goPrev(event: any) {
    this._workflow.stepBack('step20');
  }

  goRestart(event: any) {
    this._workflow.resetSteps();
  }
  goNext(event: any) {
    this._workflow.stepForward('step20', 'step21');
  }
}
