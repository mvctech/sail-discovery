import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sailstrains-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent implements OnInit {
  @Input() currentValue: string;

  constructor() {
    this.currentValue = '';
  }

  ngOnInit() {
  }

  isSet() {
    return this.currentValue !== '' ? true : false;
  }
}
