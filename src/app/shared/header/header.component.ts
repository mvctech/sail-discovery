import { Component, Input, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'sailstrains-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class HeaderComponent implements OnInit {
  @Input() cname: String;
  @Input() b1: String;
  @Input() b1route: String;
  @Input() b2: String;
  @Input() b2route: String;
  @Input() f: String;

  @Output() b1Click = new EventEmitter<MouseEvent>();
  @Output() b2Click = new EventEmitter<MouseEvent>();

  constructor() {
    this.cname = 'DEV: PASS A TITLE';
    this.b1 = '/PASS-A-ROUTE';
    this.b2 = 'Start Over';
    this.b2route = '/';
  }

  ngOnInit() {
  }

  public onb1Click(event: MouseEvent) {
    this.b1Click.emit(event);
  }

  public onb2Click(event: MouseEvent) {
    this.b2Click.emit(event);
  }

}
