import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'sailstrains-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() b1: string;
  @Input() b1route: string;
  @Input() b1function: string;
  @Input() b1disabled: boolean;

  @Output() b1Click = new EventEmitter<MouseEvent>();

  constructor() {
    this.b1 = 'Default name';
    this.b1route = '/';
    // this.b1function = 'onMovingNext();';
    this.b1disabled = true;
  }

  ngOnInit() {
  }

  onb1Click(event: MouseEvent) {
    this.b1Click.emit(event);
  }
}
