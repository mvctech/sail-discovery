import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sailstrains-collapsible',
  templateUrl: './collapsible.component.html',
  styleUrls: ['./collapsible.component.scss']
})
export class CollapsibleComponent implements OnInit {
  @Input() b1title: string;
  @Input() b1body: string;
  @Input() b1collapsed: boolean;
  b1collapsedClass: string;

  constructor() {
    this.b1collapsed = true;
    this.b1title = 'Why do we use it?';
    this.b1body = 'It is a long established fact that a reader will be' +
      ' distracted by the readable content of a page when looking at its' +
      ' layout. The point of using Lorem Ipsum is that it has a more-or-less' +
      '  normal distribution of letters, as opposed to using \'Content here,' +
      '  content here\', making it look like readable English. Many desktop' +
      '  publishing packages and web page editors now use Lorem Ipsum as' +
      ' their  default model text, and a search for \'lorem ipsum\' will' +
      ' uncover many web sites still in their infancy. Various versions have' +
      ' evolved  over the years, sometimes by accident, sometimes on purpose' +
      ' (injected humour  and the like).';
  }

  ngOnInit() {
    this.b1collapsed ? this.b1collapsedClass = 'collapsed' : this.b1collapsedClass = 'not-collapsed';
  }

  testConsoleLog() {
  }

  onDisappear() {
    this.b1collapsed = !this.b1collapsed;
    this.b1collapsed ? this.b1collapsedClass = 'collapsed' : this.b1collapsedClass = 'not-collapsed';
  }
}
