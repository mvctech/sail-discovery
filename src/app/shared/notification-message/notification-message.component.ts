import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sailstrains-notification-message',
  templateUrl: './notification-message.component.html',
  styleUrls: ['./notification-message.component.scss']
})
export class NotificationMessageComponent implements OnInit {

  @Input() message: string;
  invisible = false;

  constructor() {
    this.message = 'notification-message works! ';
  }

  ngOnInit() {
  }

  onHide() {
    this.invisible === true ? console.log(this.invisible) : this.invisible = true;
  }

}
